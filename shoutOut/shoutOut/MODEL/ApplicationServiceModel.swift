//
//  ApplicationServiceModel.swift
//  
//
//  Created by Fez on 3/21/16.
//
//

import Foundation
import UIKit
import Alamofire
import EVReflection
import INTULocationManager

let SERVICEURL = "http://184.107.143.42:900/";
let LocationURL = "http://locateapi.ShoutOutapi.com/";
let ConnectionURL = "HTTP://CONNECTIONTEST.ShoutOutAPI.COM/";


//For Menu Navigation Progamaticallyßßß
class MenuNavigation{
    let storyBoardName  : String!;
    let menuName : String!;
    let viewName : String!;
    let menuImage : UIImage!;
    
    init(menuName : String!,viewName : String!,menuImage : UIImage!){
        
        self.storyBoardName = "Main"
        self.menuName=menuName;
        self.viewName = viewName;
        self.menuImage  = menuImage;
        
    }
}

class UserPrefencesSettings: EVObject{
     var key_name: String?
     var key_value: String?
}

class SnapLocations: EVObject{
    // MARK: Properties
     var city: String?
     var ringRadius: String?
     var locationName: String?
     var indexNumber: String?
     var confidenceFactor: String?
     var postalCode: String?
     var snaptoType: String?
     var latitude: String?
     var addressLine1: String?
     var radius: NSNumber?
     var longitude: String?
     var satellitesInView: String?
     var addressLine2: String?
     var stateProvince: String?
     var altitude: String?
      var distance: NSNumber?;
}

class ValidPhoneResponse: EVObject {
    var mode  : String?;
    var sms: String?
    var status_code  : NSNumber?;
    var account_id : String?;
    var error : ValidPhoneResponseError = ValidPhoneResponseError();
}

class ValidPhoneResponseError: EVObject{
      var authorization_code  : String!;
}


class PinResponse: EVObject {
    var verified: NSNumber!;
     var session_id: String!
}

class LoggedInModel : EVObject
{
    var Authorization: String?
    var user_id: String?
    var first_name : String?;
    var last_name: String?;
    var email_address: String?
    var user_tag: String?
    var image: String?
}

class CommentModel : EVObject{
    // MARK: Properties
    var id: String?
    var user_id: String?
    var comment: String?
    var feed_id: String?
    var created_at: String?
    var updated_at: String?
    var first_name: String?
    var last_name: String?
    var image: String?
}

class CommentIdModel : EVObject{
    var comment_id: String?
}

class SearchUserModel : EVObject{
    
    var id: String?
    var first_name: String?
    var last_name: String?
    var user_tag: String?
    var image: String?

}

class SearchCityModel : EVObject{
    
    var city_name: String?
    
}


class FeedModel : EVObject{
    // MARK: Properties
    var id: String?
    var createdAt: String?
    var internalIdentifier: String?
    var message: String?
    var is_liked: String?
    var longitude: String?
    var latitude: String?
    var updatedAt: String?
    var userId: String?
    var image: String?
    var altitude: String?
    var user_image: String?
    var user_tag: String?
    var total_likes: String?
    var total_comments: String?
    var total_followers: String?
    var highlighted: Bool = false;
    var is_followed: String?
    
    
    
    
}

class FollowModel : EVObject{
    // MARK: Properties
    var id: String?
    var user_id: String?
    var following_id: String?
    var created_at: String?
    var first_name: String?
    var last_name: String?
    var image: String?
    
}







class EmailModel : EVObject {
    var email_exist : Bool = false;
}






class LocationModel : EVObject
{
    var latitude  : Double!;
    var longitude: Double!;
    var City  : String!;
    var State  : String?;
    var Country  : String!;
    var CurrentAddress : String!;
    var postalAdress : String!;
    var Street : String!;
    var Locality : String = "";
    var Area : String!;
    var lastUpdated : NSDate!;
    
}





class Help : EVObject{
    // MARK: Properties
     var timestamp: String!
     var version: String!
     var help: String!
     var helpId: String!
}

class Terms: EVObject {
     var timestamp: String!
     var termsId: String!
     var version: String!
     var termsConditions: String!
}

class Privacy: EVObject {
    var timestamp: String!
    var privacy_id: String!
    var version: String!
    var privacy_policy: String!
}

class About: EVObject {
    var timestamp: String!
    var about_id: String!
    var version: String!
    var about: String!
}

class LocationParams: EVObject{
    
    // MARK: Properties
     var speed: Double?
     var wiFiSignalStrength: Float?
     var sSIDWAP: String?
     var confidenceFactor: Float?
     var iPAddress: String?
     var accuracyRingRadius: Double?
     var bearing: Double?
     var latitude: Double?
     var neighboringCellInfo1: String?
     var mACAddressWAP: String?
     var callInProgressIndicator: String?
     var cellSiteInfo: [CellSiteInfo]?
     var ageOfGeolocation: String?
     var runMode: String?
     var longitude: Double?
     var satellitesInView: Float?
     var barometricPressure: Float?
     var typeOfCellularTech: String?
     var altitude: Double?
     var wifiInfo: [WifiInfo]?

}


public class GeoLocations : EVObject{
    var location: SingleLocation  = SingleLocation();
    var snpto_index: String!;
    var success: Bool = false
    var snap_location: SnapLocations = SnapLocations();
    var CLLoc : CLLocation!;
}




public class SingleLocation : EVObject{
    // MARK: Properties
     var address2: String!
     var city: String!
     var latitude: NSNumber!
     var matchType: String!
     var addressAdjusted: NSNumber!
     var longitude: NSNumber!
     var transactionSerialNumber: String!
     var enginesWithResults: String!
     var address1: String!
     var result: String!
     var duration: NSNumber!
     var textAddress: String!
     var postalCode: String!
     var state: String!
     var engineSelected: String!
     var enginesConsidered: String!
     var distanceFromXY: NSNumber!
     var unadjustedAddress1: String!
     //var Result: String!
     var Cause: String!
     var CauseCode: String!
     //var TransactionSerialNumber: String!
    
    
}

public class WifiInfo: EVObject {
    // MARK: Properties
    public var bSSID: String?
    public var signalStrength: Float?
}
class CellSiteInfo: EVObject{
// MARK: Properties
    
 var mCC: String?
 var cellId: String?
 var lAC: String?
 var mNC: String?
 var cellularSignalStrength: Float?

}

public class Summary: EVObject {

 // MARK: Properties
 public var message: String!
 public var code: NSNumber!
 public var success: Bool = false

}

public class ResponseObject: EVObject {
// MARK: Properties
//public var data: [AnyObject]!
public var summary: Summary!

}

public class UserInformation: EVObject {
    public var name: String?
    public var phoneNumber: String?
    public var email: String?
    public var username: String?
    public var distanceSetting: String = "Meters" ;

}
public class TermsModel: EVObject {
   // 
     var termsAccepted: Bool = false
     var newTerms: Terms?
}

public class CallInitiator: EVObject {
    //
    var method: String?;
    var pani_number: String?;
    var accuracy_limit: NSNumber?;
    var super_timer: NSNumber?;
}


public class States{
    var StateName: String?;
    var StatePrefx : String?;

    init(StateName : String!,StatePrefx : String!){

        self.StatePrefx = StatePrefx;
        self.StateName = StateName;

        
    }
    

}

