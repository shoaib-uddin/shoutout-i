//
//  UserModel.swift
//  ShoutOut
//
//  Created by Fez on 3/21/16.
//  Copyright © 2016 Matech. All rights reserved.
//
import UIKit
import Foundation

class UserModel{

    class func isUserLoggedIn() -> Bool{
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let isLoggedIn:Int = prefs.integerForKey("ISLOGGEDIN") as Int
        return (isLoggedIn == 1);
    }
    
    class func isSessionExpired() -> Bool{
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let ISEXPIREUSER:Int = prefs.integerForKey("ISEXPIREUSER") as Int
        return (ISEXPIREUSER == 1);
    }
    
    
    class func setLoginUser(response: LoggedInModel){
    
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        prefs.setInteger(1, forKey: "ISLOGGEDIN")
        prefs.setInteger(0, forKey: "ISEXPIREUSER")
        prefs.setObject(response.toJsonString(), forKey: "LoggedInInfo")
        prefs.synchronize();
   
    }
    
    
    class func setLocation(response: LocationModel){
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        prefs.setObject(response.toJsonString(), forKey: "LocationInfo")
        prefs.synchronize();
    }
    
    class func GetLocationInfo() -> LocationModel!
    {
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let LocationInfo =  prefs.objectForKey("LocationInfo");
        
        if(LocationInfo != nil){
            let model = LocationModel(json: LocationInfo as? String);
            return model;
        }
        return nil;
    }
    
    
    
    class func GetInfo() -> LoggedInModel!
    {
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let LoggedInfo =  prefs.objectForKey("LoggedInInfo") as! String!;
        let isLoggedIn =  prefs.integerForKey("ISLOGGEDIN");
        if(isLoggedIn == 1){
        let model = LoggedInModel(json: LoggedInfo);
             return model;
        }
        return nil;
    }
    
    
    
    class func setLoggedOutUser(){
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        prefs.setInteger(0, forKey: "ISLOGGEDIN")
        prefs.removeObjectForKey("LoggedInInfo");
        prefs.synchronize();
    }

    class func setExpireUser(){
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        prefs.setInteger(0, forKey: "ISLOGGEDIN")
         prefs.setInteger(1, forKey: "ISEXPIREUSER")
        prefs.removeObjectForKey("LoggedInInfo");
        prefs.synchronize();
    }
    
    
    class func setKey(KeyName : String , KeyValue : String){
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        prefs.setObject(KeyValue, forKey: KeyName)
        prefs.synchronize();
    }
    
    class func getKey(KeyName : String) -> String!{
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let KeyValue =  prefs.objectForKey(KeyName);
        if(KeyValue != nil ){
            return KeyValue! as! String;
        }
        return nil;

    }
    
   

}