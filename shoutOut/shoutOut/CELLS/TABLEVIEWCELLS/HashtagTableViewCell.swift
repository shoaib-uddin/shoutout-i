//
//  HashtagTableViewCell.swift
//  shoutOut
//
//  Created by clines193 on 7/14/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit



class HashtagTableViewCell: UITableViewCell {

    var FollowID: String!
    var indexPathRow: Int!
    
    
    
    @IBOutlet weak var imgTag: UIImageView!
    @IBOutlet weak var lblTag: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = UIColor.whiteColor();
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(title: String, img: String){
        
        self.lblTag.text = title;
        LazyImage.showForImageView(self.imgTag, url:  SERVICEURL+img)
        
        
    }

    
    
    
    
    
    
}
