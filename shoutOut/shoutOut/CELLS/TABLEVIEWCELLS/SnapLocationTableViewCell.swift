//
//  SnapLocationTableViewCell.swift
//  ShoutOut
//
//  Created by Fez on 3/28/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

@objc protocol SnapLocationCallDelegate{
    func CallFromLocation(sender: SnapLocationTableViewCell)
}
class SnapLocationTableViewCell: UITableViewCell {

  
    @IBOutlet weak var lblSnapLocation: UILabel!
    
    @IBOutlet weak var lblSnapLocationDesc: UILabel!
    var SnapID : Int = 0 ;
    weak var SnapLocationDelegate : SnapLocationCallDelegate?;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func callButtonClicked(sender: UIButton) {
        SnapLocationDelegate?.CallFromLocation(self);
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
