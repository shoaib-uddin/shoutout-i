//
//  GetAllFollowsTableViewCell.swift
//  shoutOut
//
//  Created by clines193 on 7/1/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

//@objc  protocol FollowlistCellDelegate {
//    func unFollowUser(cell: GetAllFollowsTableViewCell);
//    
//}


@objc  protocol FollowCellDelegate {
    func unFollowUser(cell: GetAllFollowsTableViewCell);
    
}


class GetAllFollowsTableViewCell: UITableViewCell {
    
    var FollowID: String!
    var indexPathRow: Int!

    @IBOutlet weak var imgProfilePicture: UIImageView!
    @IBOutlet weak var btnUnfollow: UIButton!
    @IBOutlet weak var lblCreatedAt: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    weak var cellDDelegate: FollowCellDelegate?;
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyle.None;
        smallWhiteAlternateButton(btnUnfollow, color: "green");
        roundWhiteImageBorderWidthWise(imgProfilePicture)
    }
    
    func setData(follow: FollowModel){
        
        LazyImage.showForImageView(self.imgProfilePicture, url: SERVICEURL+follow.image!)
        self.lblUserName.text = follow.first_name! + " " + follow.last_name!
        self.FollowID = follow.id;
        getdDateText(follow.created_at!);
        
        
        
        
        
    }
    
    @IBAction func unFollowUser(sender: AnyObject) {
        cellDDelegate?.unFollowUser(self);
    }
    
    
    @IBAction func btnUnfollowUser(sender: UIButton) {
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    private func getdDateText(timeString : String){
        
        let time = timeString.convertToDate(DateFormatter.postDate);
        
        var dateText : String  = "";
        let timeInterval = time.daysWithoutTimeFrom(NSDate().Local());
        
        switch timeInterval {
        case 0:
            dateText = time.ToString(DateFormatter.minuteSeconds)
        case 1:
            dateText = "Yesterday"
        case 6:
            dateText = time.ToString(DateFormatter.WeekText)
        default:
            dateText = String(timeInterval) + "  days ago";
            
        }
        //self.lblCreatedAt.text = dateText;
        //return dateText;
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
