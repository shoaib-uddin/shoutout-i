//
//  commentsTableViewCell.swift
//  shoutOut
//
//  Created by clines193 on 6/24/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit

@objc  protocol CommentlistCellDelegate {
    func DeleteMyComment(cell: commentsTableViewCell, actionType: String);
}


class commentsTableViewCell: UITableViewCell {

        
    // IBOutlets to get values
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var textAreaComment: UITextView!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var btnDeleteComment: UIButton!
    
    weak var cellDelegate: CommentlistCellDelegate?;
    
    var FeedID: String!
    var indexPathRow: Int!
    
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        setCSSLayout()
        self.selectionStyle = UITableViewCellSelectionStyle.None;
        //self.userInteractionEnabled = false;
        //self.contentView.userInteractionEnabled = false
        
        
        
        
        
            }
    
    
    func setData(Comment: CommentModel, id: String){
        
        self.lblUserName.text = Comment.first_name! + " " + Comment.last_name!
        self.lblDate.text = Comment.created_at!
        self.textAreaComment.text = Comment.comment!;
        LazyImage.showForImageView(self.imgUserProfile, url: SERVICEURL+Comment.image!);
        
        if(Comment.user_id == id){
            self.btnDeleteComment.hidden = false;
        }else{
            self.btnDeleteComment.hidden = true;
        }
        
        
        
     
        
    }
    
    
    
    func setCSSLayout(){
        
        
        self.backgroundColor = UIColor.whiteColor();
        self.backgroundView?.backgroundColor = UIColor.whiteColor();
        roundGreenImageBorder(self.imgUserProfile);
        allSideWhiteBorder(self.textAreaComment);
    }
    
    
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func DeleteButtonTapped(sender: AnyObject) {
        cellDelegate?.DeleteMyComment(self, actionType: "");
    }
    
    
    
    
    
}