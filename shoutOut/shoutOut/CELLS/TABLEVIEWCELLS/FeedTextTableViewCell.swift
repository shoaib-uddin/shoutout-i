//
//  FeedTextTableViewCell.swift
//  shoutOut
//
//  Created by clines193 on 6/16/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit

@objc  protocol FeedlistCellDelegate {
    func isFeedLiked(cell: FeedTextTableViewCell, actionType: String);
    func showComments(cell: FeedTextTableViewCell, actionType: String);
    func followThisFeed(cell: FeedTextTableViewCell, actionType: String);
    func DeleteMyShoutout(cell: FeedTextTableViewCell, actionType: String);
}

class FeedTextTableViewCell: UITableViewCell {
    
    var user_id = UserModel.GetInfo().user_id

    @IBOutlet weak var tableProfileImage: UIImageView!
    @IBOutlet weak var lblUserTagTableView: UILabel!
    @IBOutlet weak var lblDescTableView: UILabel!
    @IBOutlet weak var btnFollowFeed: UIButton!
    @IBOutlet weak var btnHeart: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var lblFeedLikes: UILabel!
    @IBOutlet weak var lblFeedcomments: UILabel!
    @IBOutlet weak var cStackView: UIStackView!
    @IBOutlet weak var btnDeleteFeed: UIButton!
    
    
    var FeedID: String!
    var indexPathRow: Int!
    var totalLikes: Int!
    var totalComments: Int!
    var totalFollowers: String!
    
    
   weak var cellDelegate: FeedlistCellDelegate?;
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyle.None;
        
    }
    
    @IBAction func DeleteThisFeed(sender: UIButton) {
        cellDelegate?.DeleteMyShoutout(self, actionType: "");
    }
    
    func DeleteThisFeed(){
        
        
        
    }
    
    func setFeedLiked(feed: FeedModel){
    
        let likeHeart = self.btnHeart;
        let feed_id = Int(self.FeedID!);
        
        dispatch_async(dispatch_get_main_queue(), {
            
            if feed.is_liked == "0"{
                
                likeHeart.highlighted = true;
                
                var c: Int? = Int(feed.total_likes!);
                c = c! + 1;
                
                ShoutoutAPI.sendLIkeorDislikeToServer(feed_id!, dislike: 0, completion: {(callback) in
                    
                    self.totalLikes = c;
                    let str = c != nil ? "\(c!)" : "0"
                    feed.total_likes = str
                    heartFontawesomeIconButton(self.btnHeart, icon: "likeed@1x", selected: true, text: str);
                    feed.is_liked = "1";
                    
                })
                
                
            }else{
                
                
                likeHeart.highlighted = false;
                
                var c: Int? = Int(feed.total_likes!);
                c = c! - 1;
                
                ShoutoutAPI.sendLIkeorDislikeToServer(feed_id!, dislike: 1, completion: {(callback) in
                    
                    self.totalLikes = c;
                    let str = c != nil ? "\(c!)" : "0"
                    feed.total_likes = str
                    heartFontawesomeIconButton(self.btnHeart, icon: "like@1x", selected: false, text: str);
                    feed.is_liked = "0";
                })
                
            }
            
            
            
        });

    
    
    }
    
    
    func setFeedFollow(feed: FeedModel){
    
        if(feed.is_followed != "1"){
            
            
            ShoutoutAPI.followFeedOfThisUser(feed.userId!, isFollow: "true", completion: {(callback) in
                feed.is_followed = "1";
                followFontawesomeIconButton(self.btnFollowFeed, icon: "follow_filled@1x", selected: true, text: "+Following");
            })
            
        }

    
    }
    
    
    
    
    
    
    
    func setData(feed: FeedModel, myshoutout: Bool){
        
        
        let image = self.tableProfileImage;
        let usertag = self.lblUserTagTableView;
        let followFeed = self.btnFollowFeed;
        let likeHeart = self.btnHeart;
        let btnComment = self.btnComment;
        
        allFeedWhiteButtons(self.btnFollowFeed)
        allFeedWhiteButtons(self.btnComment)
        allFeedWhiteButtons(self.btnHeart)
        
        setPositionElementsInButton(self.btnHeart);
        setPositionElementsInButton(self.btnComment);
        setPositionElementsInButton(self.btnFollowFeed);
        
        
        if(indexPathRow % 2 == 1){
            // odd cell
            roundGreenImageBorder(image);
            bottomBorderLabelUserTagTable(usertag);
            self.lblDescTableView.textColor = UIColor.blackColor();
            self.backgroundView?.backgroundColor = UIColor.whiteColor();
            self.backgroundColor = UIColor.whiteColor();
            
            
        }else{
            //even cell
            roundWhiteImageBorder(image);
            evenUserTagLabelColor(usertag);
            self.lblDescTableView.textColor = UIColor.whiteColor();
            self.backgroundView?.backgroundColor = UIColor.clearColor();
            self.backgroundColor = UIColor.clearColor();
            
            
            
        }

        
        
        
        
        
        
        
        
        
        
        // setting variables
        LazyImage.showForImageView(self.tableProfileImage, url: SERVICEURL+feed.user_image!)
        self.lblUserTagTableView.text = feed.user_tag;
        self.lblDescTableView.text = feed.message;
        totalLikes = Int(feed.total_likes!);
        totalFollowers = feed.total_followers
        totalComments = Int(feed.total_comments!);
        
        //self.lblFeedcomments.text = feed.total_comments!;
        self.FeedID = feed.id;
        
        let li = totalLikes != nil ? "\(totalLikes)" : "0"
        heartFontawesomeIconButton(likeHeart, icon: "like@1x", selected: false, text: li);
        
        let str = totalComments != nil ? "\(totalComments)" : "0"
        commentFontawesomeIconButton(btnComment, icon: "comment@1x", selected: false, text: str);
        
        
        
        
        
        //self.btnFollowFeed.hidden = false;
        
        if(feed.is_liked == "1"){
            heartFontawesomeIconButton(self.btnHeart, icon: "likeed@1x", selected: true, text: "\(totalLikes)");
        }else{
            heartFontawesomeIconButton(self.btnHeart, icon: "like@1x", selected: false, text: "\(totalLikes)");
        }
        
        if(user_id == feed.userId){
            btnFollowFeed.hidden = true
            (myshoutout ? (btnDeleteFeed.hidden = false) : (btnDeleteFeed.hidden = true));
            
        }else{
            btnFollowFeed.hidden = false
            btnDeleteFeed.hidden = true
        
            let fo = totalFollowers != nil ? "\(totalFollowers)" : "0"
            followFontawesomeIconButton(btnFollowFeed, icon: "follow@1x", selected: false, text: fo);
            
            if(feed.is_followed == "1"){
                followFontawesomeIconButton(self.btnFollowFeed, icon: "follow_filled@1x", selected: true, text: "+following");
            }else{
                followFontawesomeIconButton(self.btnFollowFeed, icon: "follow@1x", selected: false, text: "follow");
            }
        
        }
        
        
        
        
        
        
        
        
    }

    // like feed button action delegates
    
    @IBAction func likeButtonTapped(sender: AnyObject) {
        cellDelegate?.isFeedLiked(self, actionType: "")
    }
    
    @IBAction func commentButtonTapped(sender: AnyObject) {
        cellDelegate?.showComments(self, actionType: "")
    }
    
    @IBAction func followThisFeed(sender: AnyObject) {
        cellDelegate?.followThisFeed(self, actionType: "")
    }
    

    
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    
    
}
