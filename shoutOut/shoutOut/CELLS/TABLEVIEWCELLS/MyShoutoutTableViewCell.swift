//
//  MyShoutoutTableViewCell.swift
//  shoutOut
//
//  Created by clines193 on 6/28/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit


@objc  protocol MyShoutoutlistCellDelegate {
    func DeleteMyShoutout(cell: MyShoutoutTableViewCell, actionType: String);
}



class MyShoutoutTableViewCell: UITableViewCell {

    var FeedID: String!
    var indexPathRow: Int!
    
    @IBOutlet weak var textAreaFeed: UITextView!
    @IBOutlet weak var btnDeleteFeed: UIButton!
    @IBOutlet weak var timeAge: UILabel!
    @IBOutlet weak var imgFeedPost: UIImageView!
    
    
    weak var cellDelegate: MyShoutoutlistCellDelegate?;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        allSideWhiteBorder(textAreaFeed);
        smallAlternateButtons(btnDeleteFeed, color: "green");
        smallWhiteAlternateButton(timeAge, color: "green");
         btnDeleteFeed.contentEdgeInsets = UIEdgeInsetsMake(15, 20, 10, 10);
        
    }
    
    func setData(feed: FeedModel){
        
        self.textAreaFeed.text = feed.message;
        self.FeedID = feed.id;
        
        getdDateText(feed.createdAt!);
        
        
        
        
        
    }
    
    
    private func getdDateText(timeString : String){
        
       let time = timeString.convertToDate(DateFormatter.postDate);
        
        
        var dateText : String  = "";
        let timeInterval = NSDate().daysWithoutTimeFrom(NSDate().Local());
        
        if timeInterval ==  0 {
            dateText = time.ToString(DateFormatter.minuteSeconds)
        }
        else if timeInterval ==  1 {
            dateText = "Yesterday"
        }
        else if timeInterval <= 6  {
            dateText = time.ToString(DateFormatter.WeekText)
        }
        else{
            dateText = time.ToString(DateFormatter.Date);
        }
        self.timeAge.text = dateText;
        //return dateText;
        
        
        
        
        
    }
    
    
//    func timeDIff(createdAt: String){
//        
//        
//      
//        let sec = NSDate().Local().secondsFrom(date)
//        
//        var s = ((sec/60)/60)/24;
//        
//        if(s < 0){
//            
//            s = 0;
//            
//            
//            
//        }else{
//            self.timeAge.text = String(s) + " days age";
//            
//        }
//        
//        
//        
//    }

    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func DeleteButtonTapped(sender: AnyObject) {
        cellDelegate?.DeleteMyShoutout(self, actionType: "");
    }
    
}
