//
//  SettingsTableViewCell.swift
//  ShoutOut
//
//  Created by Fez on 3/28/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var ImgViewSettings: UIImageView!
    
    @IBOutlet weak var lblSettingsText: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
