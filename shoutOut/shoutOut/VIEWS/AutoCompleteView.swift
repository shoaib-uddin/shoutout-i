//
//  AutoCompleteView.swift
//  shoutOut
//
//  Created by clines193 on 7/14/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit


@objc  protocol AutoCompleteCellDelegate {
    func getHashtagOfUser(tag: String);
    
}

class AutoCompleteView: UIView, UITableViewDataSource, UITableViewDelegate {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
   
    var SearchUser : Array<SearchUserModel> = Array<SearchUserModel>();
    var SearchCity : Array<SearchCityModel> = Array<SearchCityModel>();
    
    var UserFlag: Bool = false
    var CityFlag: Bool = false
    
    
    
    
    
    weak var cellDDelegate: AutoCompleteCellDelegate?;
    
    @IBOutlet weak var tableView: UITableView!
    weak var cellDelegate: AutoCompleteCellDelegate?;
    
    
    
    
    
    override func awakeFromNib() {
        //
        
        tableView.registerNib(UINib(nibName: "HashtagTableViewCell", bundle: nil), forCellReuseIdentifier: "HashtagTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //tableView.backgroundView?.backgroundColor = UIColor.clearColor();
        //tableView.backgroundColor = UIColor.clearColor();
        
        
        
        
        
        
            
            
        
        
        
    }
    
    
    func setDataUserSearch(searchString: String){
        
        
        NetworkHelper.MakeGetRequestForArray("shoutout_search_users/" + "\(searchString)",showLoader: false, success: { (successData) -> Void in
            
            if(successData != "" && successData != "[\n\n]"){
            
                let response =  [SearchUserModel](json: successData)
                self.SearchUser = response;
                self.UserFlag = true;
                self.tableView.reloadData();
                
            }
            }, failure: { (error) in
                
                
        })
        
    }
    
    
    
    func setDataCitySearch(searchString: String, completion: (callback: Bool) -> Void){
        
        
        let escapedString = searchString.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet());
        let v = "\(escapedString!)"
        
        NetworkHelper.MakeGetRequestForArray("shoutout_search_city_name/" + "\(v)",showLoader: false, success: { (successData) -> Void in
            
            if(successData != "" && successData != "[\n\n]"){
                
                let response =  [SearchCityModel](json: successData)
                self.SearchCity = response;
                self.CityFlag = true;
                self.tableView.reloadData();
                completion(callback: true);
                
            }else{
                completion(callback: false);
            }
            }, failure: { (error) in
                
                
                
        })
        
    }
    
    
    
    
    //Delegation Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(SearchUser.count > 0){
            return SearchUser.count;
        }else{
            return SearchCity.count;
        }
        
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(SearchUser.count > 0){
            let name = SearchUser[indexPath.row].user_tag!
            cellDelegate?.getHashtagOfUser(name);
        }else{
            let name = SearchCity[indexPath.row].city_name!
            cellDelegate?.getHashtagOfUser(name);
        }
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("HashtagTableViewCell", forIndexPath: indexPath) as! HashtagTableViewCell
        cell.backgroundColor = UIColor.clearColor();
        cell.backgroundView?.backgroundColor = UIColor.clearColor();
        cell.indexPathRow = indexPath.row;
        
        
        if(SearchUser.count > 0 ){
            cell.setData(SearchUser[indexPath.row].user_tag!, img: SearchUser[indexPath.row].image! );
        }else{
            cell.setData(SearchCity[indexPath.row].city_name!, img: "");
        }
                        
        return cell;
        
        
        
    }
    
    
    
    
    
    
    
    

}
