//
//  PopupView.swift
//  ShoutOut
//
//  Created by Fez on 4/11/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

@objc protocol PopupActionDelegate{
    func btnCloseTapped(sender: UIButton)
    func btnActionTapped(sender: UIButton)

}


class PopupView: UIView {

   //View Level Controls
    
    weak var popUpDelegate : PopupActionDelegate?;
    
    
    @IBOutlet weak var lblpopUpText: UILabel!
    
   
    @IBOutlet weak var btnOne: UIButton!
    
    
    @IBOutlet weak var btnTwo: UIButton!
    
    @IBAction func btnTwoClicked(sender: UIButton) {
        popUpDelegate?.btnActionTapped(sender)
    }
    
    @IBAction func btnOneClicked(sender: UIButton) {
        popUpDelegate?.btnCloseTapped(sender);
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
