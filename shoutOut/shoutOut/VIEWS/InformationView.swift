//
//  InformationView.swift
//  ShoutOut
//
//  Created by Fez on 4/16/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit;

class InformationView : UIView{

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    
    override func awakeFromNib() {
        if(lblHeading != nil){
            lblHeading.adjustsFontSizeToFitWidth = true;
            
        }
          if(lblMessage != nil){
            lblMessage.adjustsFontSizeToFitWidth = true;
        }
    }
    
    @IBAction func btnCloseTapped(sender: UIButton) {
        
        let vw = sender.superview!.superview!.viewWithTag(1)! as UIView
        vw.removeFromSuperview()
        
    }
}