//
//  TabsView.swift
//  EtoCarIOS_App
//
//  Created by clines193 on 1/20/16.
//  Copyright (c) 2016 matech. All rights reserved.
//

import UIKit

@objc  protocol TabButoonDelegate {
    func TabbtnTapped(tag: Int, actionType: String)
}


class TabsView: UIView {

    
    @IBOutlet weak var btnOne: UIButton!
    @IBOutlet weak var btnTwo: UIButton!
    
    weak var cellDelegate: TabButoonDelegate?
    
    @IBAction func btnTappedView1(sender: UIButton) {
        SwitchView1Btn(sender.tag);
    }
    
    override func awakeFromNib() {
        setBtnleftBorder(btnTwo)
        // setInitialFatBottomBorder(btnOne); function not working properly
    }
    
    
    
    
    func SwitchView1Btn(tagName: Int){
        switch(tagName){
        case 1:
            
            btnOne.selected = true
            btnTwo.selected = false
            setFatBottomBorder(btnOne, v2: btnTwo, setlayer:1)
            TabbtnTapped(1)
            break;
        case 2:
            
            btnOne.selected = false
            btnTwo.selected = true
            
            setFatBottomBorder(btnOne, v2: btnTwo, setlayer:2)
            TabbtnTapped(0)
            
           
            
            break;
        default:
            break;
        }
    
    
    }
    
    func TabbtnTapped(tag: Int){
        cellDelegate?.TabbtnTapped(tag, actionType: "")
    }
   
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    

}
