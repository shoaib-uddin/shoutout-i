//
//  NotificationHelper.swift
//  EtoCarIOS_App
//
//  Created by Waqas on 2/19/16.
//  Copyright © 2016 matech. All rights reserved.
//

import Foundation
import UIKit
import EVReflection

enum NotificationType
{
}

class NotificationHelper{
    
    class func NavigateToCarOffer(RefrenceID : String,storyboard : UIStoryboard,_self : UIViewController){
        
    
    }
    
    class func NavigateToCarJob(RefrenceID : String,RefrenceName : String,var TabNum: Int,storyboard : UIStoryboard,_self : UIViewController){
    
        
    
    }
    
    
    
    
    class func SwitchView(type : String , RefrenceID : String,RefrenceName:String, _self : UIViewController){
    
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        
        switch(type){
        case "OfferChat":
            NavigateToCarOffer(RefrenceID, storyboard: storyboard, _self: _self)
            break;
        case "JobChat":
            NavigateToCarJob(RefrenceID, RefrenceName:RefrenceName,TabNum: 3,storyboard: storyboard
                , _self: _self);
            break;
            
        case "CarJobComplete":
            NavigateToCarJob(RefrenceID,RefrenceName:RefrenceName, TabNum: 0, storyboard: storyboard
                , _self: _self);
            
            
            break;
        case "CarPhotoAccepted":
            NavigateToCarJob(RefrenceID, RefrenceName:RefrenceName,TabNum: 1, storyboard: storyboard
                , _self: _self);
            break;
        case "DealerContractAccepted":
            NavigateToCarJob(RefrenceID, RefrenceName:RefrenceName,TabNum: 1, storyboard: storyboard
                , _self: _self);
            break;
        case "BrokerContractAccepted":
            
            NavigateToCarJob(RefrenceID,RefrenceName:RefrenceName, TabNum: 1, storyboard: storyboard
                , _self: _self);
            break;
        case "CarPaymentReceiptAccepted":
            NavigateToCarJob(RefrenceID, RefrenceName:RefrenceName,TabNum: 1, storyboard: storyboard
                , _self: _self);
            
            break;
        case "CarJobTransportationSubmitted":
            NavigateToCarJob(RefrenceID, RefrenceName:RefrenceName,TabNum: 1, storyboard: storyboard
                , _self: _self);
            
            break;
        case "CarPhotosUploaded":
            NavigateToCarJob(RefrenceID, RefrenceName:RefrenceName,TabNum: 1, storyboard: storyboard
                , _self: _self);
            
            break;
        case "DealerContractUploaded":
            NavigateToCarJob(RefrenceID, RefrenceName:RefrenceName,TabNum: 1, storyboard: storyboard
                , _self: _self);
            
            break;
        case "BrokerContractUploaded":
            NavigateToCarJob(RefrenceID,RefrenceName:RefrenceName, TabNum: 1, storyboard: storyboard
                , _self: _self);
            
            break;
        case "CarPaymentReceiptUploaded":
            NavigateToCarJob(RefrenceID, RefrenceName:RefrenceName,TabNum: 1, storyboard: storyboard
                , _self: _self);
            
            break;
        case "CarOfferCreated":
            NavigateToCarOffer(RefrenceID, storyboard: storyboard, _self: _self)
            
            break;
        case "CarOfferAccepted":
            NavigateToCarOffer(RefrenceID, storyboard: storyboard, _self: _self)
            
            break;
        default:
            
            break;
            
            
            
            
        
        
    }

    
    
    }
    
    class func NavigateToPushView(type : String , RefrenceID : String,RefrenceName:String, _self : UIViewController){
        
        
       // let postModel = ["id":  "\(RefrenceID)"];
        
        
      //  RequestNetwork.MakePostRequest("/notifications/read", postData: postModel, sender: _self.view) { (successData) -> Void in
            SwitchView(type, RefrenceID: RefrenceID,RefrenceName:RefrenceName, _self: _self);
        //}
           
    }
    
    
    
    
}



public class NotificationObject: EVObject {
    var  aps:Aps?;
    var  push:Push?;
    var  in_app_link:InAppLink?;
    var  extra_infor:ExtraInfor?;
    var  push_link:PushLink?;
    var  in_app:InApp?;
}

public class Aps: EVObject {
    
    var  alert : String?;
    var   category: NSNumber?;
    var  sound : String?;
}

public class Push: EVObject {
    
    var   content:String?;
}

public class InAppLink: EVObject {
    
}

public class ExtraInfor: EVObject {
    var  reference_id:String?;
    var  reference_name:String?;
    var  type:String?;
}

public class PushLink: EVObject {
}

public class InApp: EVObject {
    var  content:String?;
    var title:String?;
}

public class RootObject: EVObject {
    var  aps:Aps?;
    var  push:Push?;
    var  in_app_link:InAppLink?;
    var  extra_infor:ExtraInfor?;
    var  push_link:PushLink?;
    var  in_app:InApp?;
}


