		//
//  RequestNetwork.swift
//  EtoCarIOS_App
//
//  Created by clines193 on 1/29/16.
//  Copyright © 2016 matech. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection

class NetworkHelper {
   
    class func maximumValue<T: Comparable>(first: T, second: T) -> T
    {
        if (first >= second)
        {		
            return first
        }
        
        return second
    }
    
    class func MakePostRequest(
        Url: String,
        postData: AnyObject? = nil ,
        showLoader : Bool = true,
         timeOut : Double = 0,
        success: ((successData : AnyObject?) -> Void)!,
        failure: ((error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
            
            MakeRequest("POST", Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
            
    }
    
    class func MakeGetRequest(
        Url: String,
        postData: AnyObject? = nil ,
        showLoader : Bool = true,
         timeOut : Double = 0,
        success: ((successData : AnyObject?) -> Void)!,
        failure: ((error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
            
            MakeRequest("GET", Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
            
    }
    
 
    
    class func MakePutRequest(
        Url: String,
        postData: AnyObject? = nil ,
        showLoader : Bool = true,
         timeOut : Double = 0,
        success: ((successData : AnyObject?) -> Void)!,
        failure: ((error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
            
            MakeRequest("PUT", Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
            
    }
  
    class func MakeRequest(
        method: String,
        Url: String,
        postData: AnyObject? = nil ,
        showLoader : Bool = true,
        var timeOut : Double = 0,
        success: ((successData : AnyObject?) -> Void)!,
        failure: ((error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil,
        retry:Bool = false
        ){
        
       
        var targetUrl:String =  Url;
        if(timeOut == 0){
            timeOut  = settings.timeOut;
        }
        if(!Url.containsString("http")){
            targetUrl =  SERVICEURL + Url;
        }
        if(showLoader){
            UtilityHelper.ShowLoader();
        }
        var AllHeaders : [String: String] = [String: String]();
        if(headers != nil){
            AllHeaders = headers!;
        }
        
        if(!UtilityHelper.excludeAuthorization(targetUrl)){
            
            let model =  UserModel.GetInfo();
            if(model != nil){
                AllHeaders["Authorization"] = model.Authorization;
                
            }
            AllHeaders["api_key"] = credentials.api_key;
            AllHeaders["api_secret_key"] = credentials.api_secret_key;
            
        }
        
           LogMessage("URL : " + targetUrl, Log_Type: LogType.WEBCOM)
           LogMessage("METHOD TYPE : " + method, Log_Type: LogType.WEBCOM)
        
            let URL = NSURL(string: targetUrl)!
            let req = NSMutableURLRequest(URL: URL)
            req.HTTPMethod = method;
            req.addValue(UtilityHelper.generateDeviceToken(), forHTTPHeaderField: "Authorization")
            req.allHTTPHeaderFields = AllHeaders;
            if(postData != nil){
            req.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(postData!, options: [])
         
            let PostString = String(postData!)
            LogMessage("REQUEST DATA : " + PostString  , Log_Type: LogType.WEBCOM)
            }
            req.timeoutInterval = timeOut
        
         for header in AllHeaders {
            LogMessage("REQUEST HEADER : " + header.0 + ":" +   header.1 , Log_Type: LogType.WEBCOM)
         }
         LogMessage("REQUEST TIMEOUT TIME : " + String(timeOut) , Log_Type: LogType.WEBCOM)
        
        
        
        
        Alamofire.request(req)
            .responseJSON { response in
                
               
                
                switch response.result {
                case .Success(let JSON):
                    
                    
                   // let res =  response.response!;
                    let resObj = ResponseObject(dictionary: JSON as! NSDictionary)
                    
                    let ResponseString = resObj.toJsonString();
                    
                    LogMessage("RESPONSE DATA : " + ResponseString  , Log_Type: LogType.WEBCOM)
                    
                    if(resObj.summary.success){
                        
                        let jsonDict : NSDictionary =    JSON as! NSDictionary;
                        let data = jsonDict.objectForKey("data") as! NSDictionary!;
                        
                        success?(successData : data);
                        if(showLoader){
                        UtilityHelper.HideLoader();
                        }
                    
                    }
                    else
                    {
                        
                        if(resObj.summary.message == settings.messages.invalidCredentialsText){
                             UtilityHelper.HideLoader();
                            let topController = UIApplication.topViewController()!;
                            UtilityHelper.AlertMessagewithCallBack(settings.messages.sessionExpired, success: { () -> Void in
                                UserModel.setExpireUser();
                          
                                let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                let destination = storyboard.instantiateViewControllerWithIdentifier("SplashViewController") as UIViewController
                                topController.presentViewController(destination, animated: true) { () -> Void in
                                }

                                
                            })
                            
                        }
                        else{
                            if(showLoader){
                                UtilityHelper.AlertMessage(resObj.summary.message);
                            }
                            
                            failure?(error: JSON);
                            
                            if(showLoader){
                                UtilityHelper.HideLoader();
                            }
                        }

                    }
                    
                case .Failure(let error):
                    print(error.code)
                    LogMessage("RESPONSE DATA : ERROR: - ERROR CODE " + String(error.code) , Log_Type: LogType.WEBCOM)
                    LogMessage("RESPONSE DATA : ERROR: " + error.description , Log_Type: LogType.WEBCOM)
                    
                    if(error.code == settings.messages.errorCode &&  error.localizedDescription == settings.messages.timeOutErrorMesasge && Url != ConnectionURL){
                        if(!retry){
                            MakeRequest(method, Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers,retry:true);
                        }
                        else{
                            ErrorinConnection = true;
                        }
                    }
                    else{
                        failure?(error: error);
                        
                    
                    }
                    if(showLoader){
                        
                        //UtilityHelper.AlertMessage("Unable to connect to our servers. Please make sure you have an internet connection.")
                        
                        UtilityHelper.HideLoader();
                    }
                    
                    
        }
               
        
        }
    
    }
    
    
    
    
    class func MakePostRequestForArray(
        Url: String,
        postData: AnyObject? = nil ,
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((successData : String) -> Void)!,
        failure: ((error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
        
        MakeRequestForArray("POST", Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }

    class func MakeGetRequestForArray(
        Url: String,
       postData: AnyObject? = nil ,
        showLoader : Bool = true,
         timeOut : Double = 0,
        success: ((successData : String) -> Void)!,
        failure: ((error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
            
            MakeRequestForArray("GET", Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
            
    }
    
  
    class func MakePutRequestForArray(
        Url: String,
        postData: AnyObject? = nil ,
        showLoader : Bool = true,
         timeOut : Double = 0,
        success: ((successData : String) -> Void)!,
        failure: ((error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
            
            MakeRequestForArray("PUT", Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
            
    }
    
    class func MakeRequestForArray(
        method: String,
        Url: String,
        postData: AnyObject? = nil ,
        showLoader : Bool = true,
        var timeOut : Double = 0,
        success: ((successData : String) -> Void)!,
        failure: ((error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil,
        retry:Bool = false)
     {
            
        var targetUrl:String =  Url;
        
        if(timeOut == 0){
            timeOut  = settings.timeOut;
        }
        if(!Url.lowercaseString.containsString("http")){
            
            targetUrl =  SERVICEURL + Url;
            
        }
        if(showLoader){
            UtilityHelper.ShowLoader();
        }
        var AllHeaders : [String: String] = [String: String]();
        
        if(headers != nil){
            AllHeaders = headers!;
        }
        
        if(!UtilityHelper.excludeAuthorization(targetUrl)){
            
            let model =  UserModel.GetInfo();
            if(model != nil){
                AllHeaders["Authorization"] = model.Authorization;
                
            }
            AllHeaders["api_key"] = credentials.api_key;
            AllHeaders["api_secret_key"] = credentials.api_secret_key;
            
            
        }
        
        LogMessage("URL : " + targetUrl, Log_Type: LogType.WEBCOM)
        LogMessage("METHOD TYPE : " + method, Log_Type: LogType.WEBCOM)
        
        let URL = NSURL(string: targetUrl)!
        let req = NSMutableURLRequest(URL: URL)
        req.HTTPMethod = method;
        req.addValue(UtilityHelper.generateDeviceToken(), forHTTPHeaderField: "Authorization")
        req.allHTTPHeaderFields = AllHeaders;
        if(postData != nil){
            req.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(postData!, options: [])
            let PostString = String(postData!)
            LogMessage("REQUEST DATA : " + PostString  , Log_Type: LogType.WEBCOM)
            
        }
          req.timeoutInterval = timeOut
        for header in AllHeaders {
            LogMessage("REQUEST HEADER : " + header.0 + ":" +   header.1 , Log_Type: LogType.WEBCOM)
        }
        
        
        LogMessage("REQUEST TIMEOUT TIME : " + String(timeOut) , Log_Type: LogType.WEBCOM)
        
        Alamofire.request(req)
                .responseString { response in
                    switch response.result {
                    case .Success(let JSON):
                        
                        LogMessage("RESPONSE DATA : SUCCESS: " + JSON , Log_Type: LogType.WEBCOM)
                        
                        let resObj = ResponseObject(json: JSON)
                        if(resObj.summary == nil){
                          success?(successData : JSON);
                          return;
                        }
                        
                        if(resObj.summary.success ){
                            
                            let jsonDict : NSDictionary =   JSON.ToDictionary()!;
                            let data = jsonDict.objectForKey("data") as! NSArray;
                            
                            success?(successData : data.ToString());
                            if(showLoader){
                                UtilityHelper.HideLoader();
                            }
                            
                        }
                        else
                        {
                            if(resObj.summary.message == settings.messages.invalidCredentialsText){
                                UtilityHelper.HideLoader();
                                let topController = UIApplication.topViewController()!;
                                UtilityHelper.AlertMessagewithCallBack(settings.messages.sessionExpired, success: { () -> Void in
                                    UserModel.setExpireUser();
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                    let destination = storyboard.instantiateViewControllerWithIdentifier("SplashViewController") as UIViewController
                                    topController.presentViewController(destination, animated: true) { () -> Void in
                                    }
                                    
                                    
                                })
                                
                            }
                            else{
                            if(showLoader){
                                UtilityHelper.AlertMessage(resObj.summary.message);
                            }
                            
                            failure?(error: JSON);
                            
                            if(showLoader){
                                UtilityHelper.HideLoader();
                            }
                            }
                        }
                        
                    case .Failure(let error):
                        print(error.code)
                        LogMessage("RESPONSE DATA : ERROR: - ERROR CODE " + String(error.code) , Log_Type: LogType.WEBCOM)
                        LogMessage("RESPONSE DATA : ERROR: " + error.description , Log_Type: LogType.WEBCOM)
                        
                        if(error.code == settings.messages.errorCode &&  error.localizedDescription == settings.messages.timeOutErrorMesasge  && Url != ConnectionURL){
                            if(!retry){
                                MakeRequestForArray(method, Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers, retry:true)

                            
                            }
                            else{
                                ErrorinConnection = true;
                            }
                        }
                        else{
                            failure?(error: error);
                          
                            
                        }
                        if(showLoader){
                            
                            //UtilityHelper.AlertMessage("Unable to connect to our servers. Please make sure you have an internet connection.")
                            
                            UtilityHelper.HideLoader();
                        }
                        
                    }
                    
                    
            }
            
    }

    
    public class func GeneralRequest(
        method: String,
        Url: String,
        postData: [String: String]? = nil ,
        showLoader : Bool = true,
        success: ((successData : String) -> Void)!,
        failure: ((error : AnyObject?) -> Void)!)
    {
        
        var targetUrl:String =  Url;
        
         Alamofire.request(.POST, targetUrl, parameters: postData).responseString { response in
          switch response.result {
            case .Success(let JSON):
           success?(successData : JSON);
            print("Response String: \(response.result.value)")
            break;
        
        case .Failure(let error):
        print(error.code);
        LogMessage("RESPONSE DATA : ERROR: - ERROR CODE " + String(error.code) , Log_Type: LogType.WEBCOM)
        LogMessage("RESPONSE DATA : ERROR: " + error.description , Log_Type: LogType.WEBCOM)
        
        if(showLoader){
        // UtilityHelper.AlertMessage("Unable to connect to our servers. Please make sure you have an internet connection.")
        UtilityHelper.HideLoader();
        }
     
        }
        
           }
    }
    
}