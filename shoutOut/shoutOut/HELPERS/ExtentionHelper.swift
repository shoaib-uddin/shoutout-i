//
//  DataHelper.swift
//  ShoutOut
//
//  Created by Fez on 4/27/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit



extension NSDate
{
    func  Local() -> NSDate{
        
        let offsetTime = NSTimeInterval(NSTimeZone.localTimeZone().secondsFromGMT)
        let Date: NSDate = NSDate(timeInterval: offsetTime, sinceDate: self)
        return Date;
    }
    
    
    
    func ToString(Format: String) -> String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = Format
        dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let timeStamp = dateFormatter.stringFromDate(self)
        
        return timeStamp
    }
    func yearsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    func monthsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    func weeksFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    func daysFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    func hoursFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    func minutesFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    func secondsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
    func milliSecondsFrom(date:NSDate) -> Double{
        return  NSDate().timeIntervalSinceDate(date)
        
    }
    
    func offsetFrom(date:NSDate) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date))y"   }
        if monthsFrom(date)  > 0 { return "\(monthsFrom(date))M"  }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date))w"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date))d"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date))h"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date))m" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))s" }
        return ""
    }
    
    func daysWithoutTimeFrom(date:NSDate) -> Int{
        
    
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        
        // Replace the hour  of both dates with 00:00
        let date1 = calendar.startOfDayForDate(date)
        let date2 = calendar.startOfDayForDate(self.Local())
        
        return calendar.components(.Day, fromDate: date1, toDate: date2 , options: []).day
    
    
    
    
    }
    
    
}



extension NSBundle {
    
    var releaseVersionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {
        return self.infoDictionary?["CFBundleVersion"] as? String
    }
    var VersionName: String? {
        return self.infoDictionary?["CFBundleIdentifier"] as? String
    }
}

extension NSNumber{
    
    func ToString(round :Int) -> String{
        let vale: Double = Double(self)
        let formatter: NSNumberFormatter = NSNumberFormatter()
        formatter.maximumFractionDigits = round
        formatter.minimumFractionDigits = round
        let result: String = formatter.stringFromNumber(vale)!
        return result;
        
    }
    
}

extension Double{
    
    func ToString(round :Int) -> String{
        let vale: Double = Double(self)
        let formatter: NSNumberFormatter = NSNumberFormatter()
        formatter.maximumFractionDigits = round
        formatter.minimumFractionDigits = round
        let result: String = formatter.stringFromNumber(vale)!
        return result;
        
    }
    
}

extension NSDictionary
{
    func ToString() ->String
    {
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(self, options: NSJSONWritingOptions.PrettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
            return jsonString;
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            print(error)
        }
        return "";
    }
    
}

extension NSArray
{
    
    func ToString() ->String
    {
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(self, options: NSJSONWritingOptions.PrettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
            return jsonString;
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            print(error)
        }
        return "";
    }
    
}



extension String {
    
    func ToDictionary() -> [String:AnyObject]? {
        if let data = self.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    
    func ToNSAttributedString(fontSize : CGFloat = 20.0, showBold: Bool = false  ) -> NSAttributedString!{
        
        do {
            let str = try NSAttributedString(data: self.replace("\n", withString: "").replace("\t", withString: "").dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            
            //let fontSize: CGFloat = 20.0
            let fontFamily: String = "Open Sans"
            var font = UIFont(name: fontFamily, size: fontSize)!;
            let attrString = NSMutableAttributedString(attributedString: str)
            if(showBold){
                font = font.bold();
            }
            // attrString.addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, attrString.length))
            
            
            
            return attrString;
            
        } catch {
            print(error)
        }
        return nil;
    }
    
    
    func convertToDate(FormatterStr: String)->NSDate{
        
        let dateString = self // "2014-07-15" // change to your date format
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = FormatterStr //
        
        let date = dateFormatter.dateFromString(dateString)
        
        return date!;
    }

    
    
    func replace(target: String, withString: String) -> String
    {
        return self.stringByReplacingOccurrencesOfString(target, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func EncodeURL() -> String{
        
        return self.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    }
    
    func ReplaceWithStar() -> String{
        let item = self;
        var value = "";
        var firstProcessed = false;
        for chr in item.characters {
            
            if(firstProcessed){
                value = value + "*";
            }
            else{
                value  = value + String(chr);
                firstProcessed = true;
            }
        }
        return value;
    }
    
    
}

extension UIFont {
    
    func withTraits(traits:UIFontDescriptorSymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor().fontDescriptorWithSymbolicTraits(UIFontDescriptorSymbolicTraits(traits))
        return UIFont(descriptor: descriptor, size: 0)
    }
    
    func bold() -> UIFont {
        return withTraits(.TraitBold)
    }
    
    func italic() -> UIFont {
        return withTraits(.TraitItalic)
    }
    
    func boldItalic() -> UIFont {
        return withTraits(.TraitBold, .TraitItalic)
    }
    
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

extension UIImage {
    func makeImageWithColorAndSize(color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRectMake(0, 0, size.width, size.height))
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

extension NSObject{
    public class var nameOfClass: String{
        return NSStringFromClass(self).componentsSeparatedByString(".").last!
    }
    
    public var nameOfClass: String{
        return NSStringFromClass(self.dynamicType).componentsSeparatedByString(".").last!
    }
}



