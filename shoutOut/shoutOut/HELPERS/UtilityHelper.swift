//
//  UtilityHelper.swift
//  ShoutOut
//
//  Created by Fez on 3/21/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import LKAlertController
import AVFoundation
import INTULocationManager;
import ReachabilitySwift;

class UtilityHelper
{
    
    class func ShowLoader(title:String = "Loading") {
        let window: UIWindow! = UIApplication.sharedApplication().windows.last;
        MBProgressHUD.hideAllHUDsForView(window, animated: true)
        let hud: MBProgressHUD = MBProgressHUD.showHUDAddedTo(window, animated: true)
        hud.labelText = title;
        hud.show(true)
    
    }
    
    class func HideLoader() {
        let window: UIWindow! = UIApplication.sharedApplication().windows.last
        MBProgressHUD.hideHUDForView(window, animated: true)
    }
    
    class func AlertMessage(msg: String) {
        Alert(title: nil, message: msg)
         .addAction("Ok")
          .show()
    }
    
    class func AlertMessagewithCallBack(msg: String,  success: () -> Void) {
       Alert(title: nil, message: msg).addAction("OK", style: .Default) { (_) -> Void in
            success();
        }.show();
        
    }
    
    
    class func excludeAuthorization(ReqUrl:String)-> Bool {
        var ret:Bool=false;
        let ExcludeElements:[String] = [
            //"login",
        ]
        
        let check=ExcludeElements.filter({ el in ReqUrl.rangeOfString(el) != nil  });
        ret=(check.count > 0);
        return ret;
    }
    
    class func NavigateToViewController(_self:UIViewController,viewControllerid: String){
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let destination = storyboard.instantiateViewControllerWithIdentifier(viewControllerid) as UIViewController
        _self.navigationController?.pushViewController(destination, animated: true)
    
    }
    
    class func NavigateToDashboard(_self:UIViewController)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let destination = storyboard.instantiateViewControllerWithIdentifier("DashboardTabBarController") as! UITabBarController
        _self.presentViewController(destination, animated: true) { () -> Void in
        }
        
    }
    
    class func NavigatetoNavigationController(_self:UIViewController,ViewStoryBoardID : String = "Main")
    {
        let navigationController : UINavigationController = (_self.storyboard?.instantiateViewControllerWithIdentifier(ViewStoryBoardID) as? UINavigationController)!
        _self.presentViewController(navigationController, animated: true, completion: nil)
    
    }
    
    class func generateDeviceToken() ->String{
        
        let device:UIDevice = UIDevice.currentDevice();
      
        let AppID = NSBundle.mainBundle().VersionName! as String + "|";
        let AppVersion = NSBundle.mainBundle().buildVersionNumber! + "|";
        let DeviceID :  String  = device.identifierForVendor!.UUIDString as String + "|";
        let DeviceName :  String  = UIDevice.currentDevice().name as String + "|" ;
        let CurrentTimeStamp = String(NSDate().timeIntervalSince1970)
        
        let APISecurityInfo = AppID  + AppVersion  + DeviceID  + DeviceName +  CurrentTimeStamp
        return APISecurityInfo;
        //AccessToken = Encrypt(APISecurityInfo, {PublicKey})
    
    }
    
    class func getIpAddress() -> String{
        var IpAddress = "";
        let host = CFHostCreateWithName(nil,"www.google.com").takeRetainedValue()
        CFHostStartInfoResolution(host, .Addresses, nil)
        var success: DarwinBoolean = false
        if let addresses = CFHostGetAddressing(host, &success)?.takeUnretainedValue() as NSArray?,
            let theAddress = addresses.firstObject as? NSData {
                var hostname = [CChar](count: Int(NI_MAXHOST), repeatedValue: 0)
                if getnameinfo(UnsafePointer(theAddress.bytes), socklen_t(theAddress.length),
                    &hostname, socklen_t(hostname.count), nil, 0, NI_NUMERICHOST) == 0 {
                        if let numAddress = String.fromCString(hostname) {
                           IpAddress = numAddress
                        }
                }
        }
        return IpAddress;
    }
    
    class func disableTextBox(textfield: UITextField){
        textfield.enabled = false;
        textfield.textColor = UIColor.grayColor();
        
    }
    
    class func SendLocalNotification(message : String!){
        let localNotification = UILocalNotification()
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 5)
        localNotification.alertBody = message
        localNotification.timeZone = NSTimeZone.defaultTimeZone()
        
        localNotification.applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
        
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
    }
    class func customLog(lbldebugSteps: UILabel,message: String, append:Bool = false){
        dispatch_async(dispatch_get_main_queue()) {
            NSLog(message);
            
            if(append){
                lbldebugSteps.text =  lbldebugSteps.text! as String!  + message;
            }
            else
            {
                lbldebugSteps.text = message;
                
            }
            
        };
        
    }
    
//    class func metertoFoot(meter : Double) -> Double
//    {
//        return  (meter )/3;
//    }
    class func metertoFeet(meter : Double) -> Double
    {
        let feet =  (meter )  / 0.3048;
        return feet;
        
    }
    class func Feettometer(feet : Double) -> Double
    {
        let meter =  (feet  )  *  0.3048;
        return meter;
        
    }
    
    class func ImageToBase64String(theImage: UIImage) -> String{
        let imageData :  NSData = UIImageJPEGRepresentation(theImage, 0.40)!
        
        let encodedString: String =  imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        
        
        
        return encodedString;
        
    }
}

func log(text:String) {
   // #if DEBUG
//        let file = "dump.txt"//this is the file. we will write to and read from it
//        
//        if let dir = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
//            let path = NSURL(fileURLWithPath: dir).URLByAppendingPathComponent(file)
//            
//            //writing
//            do {
//                try text.writeToURL(path, atomically: false, encoding: NSUTF8StringEncoding)
//            }
//            catch {
//                /* error handling here */
//            }
//            
//        }
   // #endif
    NSLog(text);
}

func colorWithHexString (hex:String) -> UIColor {
    var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
    
    if (cString.hasPrefix("#")) {
        cString = (cString as NSString).substringFromIndex(1)
    }
    
    if (cString.characters.count != 6) {
        return UIColor.grayColor()
    }
    
    let rString = (cString as NSString).substringToIndex(2)
    let gString = ((cString as NSString).substringFromIndex(2) as NSString).substringToIndex(2)
    let bString = ((cString as NSString).substringFromIndex(4) as NSString).substringToIndex(2)
    
    var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
    NSScanner(string: rString).scanHexInt(&r)
    NSScanner(string: gString).scanHexInt(&g)
    NSScanner(string: bString).scanHexInt(&b)
    
    
    return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
}




// TableView Delegation Methods
func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("SettingsTableViewCell", forIndexPath: indexPath) as
    UITableViewCell
    return cell;
    
}

func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10;
}

func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1;
}

func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    
}
// End TableView Delegation Methods


