//
//  DeviceUtility.swift
//  ShoutOut
//
//  Created by Fez on 26/04/2016.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit
import INTULocationManager;
import ReachabilitySwift;
import CoreMotion;
import SystemConfiguration;
import Foundation;
import CoreTelephony;

private let sharedUtil = DeviceUtility()


class DeviceUtility: NSObject {
    
        private var reachabilityRef: SCNetworkReachability?
        private var reachabilityFlags: SCNetworkReachabilityFlags {
        
        guard let reachabilityRef = reachabilityRef else { return SCNetworkReachabilityFlags() }
        
        var flags = SCNetworkReachabilityFlags()
        let gotFlags = withUnsafeMutablePointer(&flags) {
            SCNetworkReachabilityGetFlags(reachabilityRef, UnsafeMutablePointer($0))
        }
        
        if gotFlags {
            return flags
        } else {
            return SCNetworkReachabilityFlags()
        }
    }
    
    
    var reachability: Reachability!
    var altimeter: CMAltimeter!

    class var sharedInstance: DeviceUtility {
            return sharedUtil
    }

    func checkGPSState () -> Bool {
        
        var isGpsOn : Bool!
        
        let state = INTULocationManager.locationServicesState()
        
        if state == INTULocationServicesState.Available{
            isGpsOn = true
        }
        else{
            isGpsOn = false
        }
        
        
        return isGpsOn
        
    }
    
    func checkWifiState () -> Bool {
        
        
        do {
            if reachability == nil {
                reachability = try Reachability.reachabilityForInternetConnection()
            }
        } catch {
            print("Unable to create Reachability")
            
        }
        reachability.isReachableViaWWAN()
        print(reachability!.isReachableViaWiFi())
        
        return reachability!.isReachableViaWiFi()
        
     }
    
    func checkIfCellularDataIsOn () -> Bool {
        
        
        do {
            if reachability == nil {
                reachability = try Reachability.reachabilityForInternetConnection()
            }
        } catch {
            print("Unable to create Reachability")
            
        }
        
        print(reachability!.isReachableViaWWAN())
        
        return reachability!.isReachableViaWWAN()
        
    }
    func canDevicePlaceAPhoneCall() -> Bool {
        /*
        
        Returns YES if the device can place a phone call
        
        */
        // Check if the device can place a phone call
        if UIApplication.sharedApplication().canOpenURL(NSURL(string: "tel://")!) {
            // Device supports phone calls, lets confirm it can place one right now
            let netInfo: CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
            let carrier: CTCarrier = netInfo.subscriberCellularProvider!
            let mnc = carrier.mobileNetworkCode
            if (carrier.mobileNetworkCode != nil){
            if (mnc!.characters.count == 0) || ((mnc == "65535")) {
                print(mnc);
                // Device cannot place a call at this time.  SIM might be removed.
                return false
            }
            else {
                // Device can place a phone call
                return true
            }
            }
            else{
                return false;
            }
        }
        else {
            // Device does not support phone calls
            return false
        }
    }
    
    
    func checkInternetReachable () -> Bool {
        
        
        do {
            if reachability == nil {
                reachability = try Reachability.reachabilityForInternetConnection()
            }
        } catch {
            print("Unable to create Reachability")
            
        }

        return reachability!.isReachable()
        
    }
    
    func ShowUserSettings()
    {
        UIApplication.sharedApplication().openURL(NSURL(string:UIApplicationOpenSettingsURLString)!);
    }
    
    func trackAltitude() {
        if altimeter == nil {
            self.altimeter = CMAltimeter()
        }
        
        // check if the barometer is available
        if !CMAltimeter.isRelativeAltitudeAvailable() {
            return
        }
        else {
            
        }
        // start altitude tracking
        self.altimeter.startRelativeAltitudeUpdatesToQueue(NSOperationQueue.mainQueue()) { (data, error) -> Void in
             self.fetchReadings(data!)
        }
    }
    
    func fetchReadings(altitudeData: CMAltitudeData) {
        let formatter: NSNumberFormatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumIntegerDigits = 1
        let timestamp: Int = Int(altitudeData.timestamp)
        
        //Barometer reading
        let timeInterval: String = "\(formatter.stringFromNumber(timestamp))"
        let altitude: String = "\(formatter.stringFromNumber(altitudeData.relativeAltitude))"
        let pressure: String = "\(formatter.stringFromNumber(altitudeData.pressure))"
      
    }
    
    func isSkypeInstalled() -> Bool{

        let isInstalled = UIApplication.sharedApplication().canOpenURL(NSURL(string: "skype:")!)

        return isInstalled
    }
    
    func isWatsappInstalled() -> Bool{
        
        let isInstalled = UIApplication.sharedApplication().canOpenURL(NSURL(string: "whatsapp://send?text=Hello%2C%20World!")!)
        
        return isInstalled
    }
   
    func isViberInstalled() -> Bool{
        
        let isInstalled = UIApplication.sharedApplication().canOpenURL(NSURL(string: "viber://url")!)
        
        return isInstalled
    }
    
    func isPSTNAvailable()->Bool{
        
        let url:NSURL? = NSURL(string: "tel://9809088798")
        
        let isInstalled = UIApplication.sharedApplication().canOpenURL(url!)
        
        return isInstalled
    }
}
