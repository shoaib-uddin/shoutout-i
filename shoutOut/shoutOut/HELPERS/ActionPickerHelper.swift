//
//  ActionPickerHelper.swift
//  ShoutOut
//
//  Created by Fez on 3/31/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit
import ActionSheetPicker_3_0;

class ActionPickerHelper {
    

 class func dropDownWithOutDone(  dataArray: [String],sender : UIButton, completion: ((TextData : String) -> Void)!, selectTitle:String = "Select"){
    
    
    
    
    ActionSheetStringPicker.showPickerWithTitle(selectTitle, rows:dataArray, initialSelection: 1, doneBlock: {
        picker, value, index in
        
        print("value = \(value)")
        print("index = \(index)")
        print("picker = \(picker)")
        
        
        completion?(TextData: index as! String!)
        
        
        return
        
        }, cancelBlock: { ActionStringCancelBlock in return }, origin: sender)
    
    
    
}


    class func dropDown(  dataArray: [AnyObject],sender : UIButton, completion: ((TextData : AnyObject, indexNumber: Int) -> Void)!, selectTitle:String = "Select", _InitialSelection:Int = 1){
    
        
        
        
        
    ActionSheetStringPicker.showPickerWithTitle(selectTitle, rows:dataArray, initialSelection: _InitialSelection, doneBlock: {
        picker, index, value  in
        
        print("value = \(value)")
        print("index = \(index)")
        print("picker = \(picker)")
        
        
        completion?(TextData: value , indexNumber : index)
        
        
        return
        
        }, cancelBlock: { ActionStringCancelBlock in return }, origin: sender)
    
    
    
}
}