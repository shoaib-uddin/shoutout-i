//
//  LocationHelper.swift
//  ShoutOut
//
//  Created by Fez on 4/14/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import INTULocationManager
import Alamofire
import EVReflection
import MapKit


class LocationHelper  : NSObject{
class func UpdateUserLocation(_loc : CLLocation,
    success: ((ServerLocation : GeoLocations?) -> Void)!,
    failure: ((error : AnyObject?) -> Void)!
    ){
      
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            // do your task
            
        dispatch_async(dispatch_get_main_queue()) {
        if(UserModel.isUserLoggedIn()){
          
            let postModel = [
                "speed" :"\(_loc.speed)",
                "Run_mode" :"test",
                "bearing" :"\(_loc.course)",
                "confidence_factor" :"24.20202",
                "Age_of_Geolocation"  :"24.20202",
                "Neighboring_cell_info1" : "test",
                "IP_address" :"",//"\(UtilityHelper.getIpAddress())",
                "longitude" : _loc.coordinate.longitude,
                "satellites_in_view" :_loc.coordinate.longitude,
                "latitude" :_loc.coordinate.latitude,
                "wifi_info" : [],
                "cell_site_info" : [],
                "MAC_address_WAP" : "test",
                "barometric_pressure" : 0,
                "Type_of_cellular_tech" : "test",
                "altitude" : _loc.altitude,
                "accuracy_ring_radius" : _loc.horizontalAccuracy,
                "Call_in_progress_indicator" : "test"
            ]
            
            ParamHorizontalAccuracy = _loc.horizontalAccuracy;
            pendingRequest = true;
            NetworkHelper.MakePutRequest("ShoutOut_report_geoinfo/", postData: postModel, showLoader: false, timeOut: 5 ,success: { (successData) -> Void in
                
                pendingRequest = false;
                
                let GeoLocation :GeoLocations =  GeoLocations(dictionary :successData as! NSDictionary);
                GeoLocation.CLLoc = _loc;
                LastLocation = GeoLocation;
                success?(ServerLocation: GeoLocation);
                }, failure: { (error) -> Void in
                    failure?(error:error);
                    pendingRequest = false;
                    
                    
            })
        }
            }
        }
            
        
}
    
    class func openMapForPlace(lat:Double, long:Double,venueName : String) {
        
        let latitute:CLLocationDegrees =  lat
        let longitute:CLLocationDegrees =  long
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitute, longitute)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(venueName)"
        mapItem.openInMapsWithLaunchOptions(options)
        
    }
    
    class func lasser_init(
         success: ((TermsModel : TermsModel?) -> Void)!
        )
    {
        if(UserModel.isUserLoggedIn()){
            
            let postModel =
            ["device_os" : UIDevice.currentDevice().type.rawValue,
                "os_version" :  UIDevice.currentDevice().systemVersion,
                "GPS" : "test1",
                "wifi" : "test1",
                "barometer" : "test1",
                "cellular_data" : "test1",
                "skype" : "test1",
                "PSTN" : "test1",
                "viber" : "test1",
                "whatsapp" : "test1"];
         
            
            
            //Network Request
            LogMessage("Calling ShoutOut_init Method ", Log_Type: LogType.MAIN)
            NetworkHelper.MakePostRequest("/ShoutOut_init/en/", postData: postModel, showLoader: false, success: { (successData) -> Void in

                if( successData != nil){
                let response =  TermsModel(dictionary: successData as! NSDictionary)
                
                    NSNotificationCenter.defaultCenter().postNotificationName(settings.Notificationkeys.InitIdentifier, object: response)
                    
                    success?(TermsModel: response);
                    
             //   if(response.termsAccepted == false)
               // {

                    
//                    let screenSize : CGRect = UIScreen.mainScreen().bounds;
//                    let FrameSize : CGRect = CGRectMake(0,0,screenSize.width,screenSize.height)
//                    let backgroundView : UIView = UIView(frame: FrameSize)
//                    backgroundView.tag = 1;
//                    backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
//                    let window: UIWindow! = UIApplication.sharedApplication().delegate!.window!
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
////                    let vw : TermsOfUseViewController = (storyboard.instantiateViewControllerWithIdentifier("TermsOfUseViewController") as? TermsOfUseViewController)!
//                    
//                    let vw: TermsAndConditionsView =  NSBundle.mainBundle().loadNibNamed("TermsAndConditionsView", owner: self, options: nil)[0] as! TermsAndConditionsView
//                    
//                    vw.frame = CGRectMake(20,20 , screenSize.size.width - 40, vw.frame.size.height - 40 )
//                    backgroundView.addSubview(vw);
//                    window.addSubview(backgroundView)
              //  }
                }
                else{
                    success?(TermsModel: nil);
                }
                },failure: { (error) -> Void in
                      success?(TermsModel: nil);
            })
            
        }
    }

    
    
}