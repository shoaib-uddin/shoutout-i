//
//  LogHelper.swift
//  ShoutOut
//
//  Created by Fez on 5/19/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit
private let sharedUtil = Log()

public enum LogType : String
{
case WEBCOM = "WEBCOM"
case LOC = "LOC"
case MAIN = "MAIN"
case CALL = "CALL"
}
var WritingLog = false;

public func LogMessage(let message:String, let Log_Type:LogType, show:Bool = false)
{
    //if(show){
    Log.Write(message, Log_Type: LogType.MAIN);
    //}
}

public class Log
{
    
//    class var sharedInstance: Log {
//        return sharedUtil
//    }
    
    public class func Write(let text:String, let Log_Type:LogType){
    
        //Network Request
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            // do your task
            
            dispatch_async(dispatch_get_main_queue()) {

        
        let file = General.logFileName; //this is the file. we will write to and read from it
        var  textdata = text;
      
        textdata =  NSDate().ToString(DateFormatter.fullDate) + " -- " + String(Log_Type.rawValue) +  " -- " + textdata
                
       // print(textdata);
        if let dir = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
            let path = NSURL(fileURLWithPath: dir).URLByAppendingPathComponent(file)
            //writing
            do {
              try textdata.appendLineToURL(path)
      
            
            let size : UInt64 = getFileSize(path)
            if(size >= (200 * 1024)){ //024
                
                if(!WritingLog){
                    WritingLog = true;
                postLogToServer(path, completed: { (success) in
                    " ".WriteLineToURLNorm(path);
                })
                }
              
            
            }
            //NSLog(Read(path))
            }
            catch {
                /* error handling here */
            }
            }
            }
            
            }
            
    
    }
    
    public class func Read(path: NSURL) -> String{
        //reading
        var text  = "";
        do {
            text = try NSString(contentsOfURL: path, encoding: NSUTF8StringEncoding) as String
        }
        catch {/* error handling here */}
        return text;
        
    }
    
    public class func ReadLog() -> String{
        
        let file = General.logFileName; //this is the file. we will write to and read from it
        
        //reading
        var text  = "";
        
        if let dir = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
            let path = NSURL(fileURLWithPath: dir).URLByAppendingPathComponent(file)
            //writing
            do {
       
                text = try NSString(contentsOfURL: path, encoding: NSUTF8StringEncoding) as String
        }
        catch {/* error handling here */}
        
        }
        return text;
        
    }
    
    
    public class func getFileSize(path: NSURL) -> UInt64{
        var  fileSize: UInt64 = 0
        
        do {
            let attr : NSDictionary? = try NSFileManager.defaultManager().attributesOfItemAtPath(path.path!)
       
            if let _attr = attr {
                fileSize = _attr.fileSize();
            }
        } catch {
            print("Error: \(error)")
        }
        return fileSize;
    }
    
    public class func postLog(
           completed: ((successData : Bool) -> Void)!
        ){
        
                let file = General.logFileName; //this is the file. we will write to and read from it
                if let dir = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
                    let path = NSURL(fileURLWithPath: dir).URLByAppendingPathComponent(file)
                    //writing
                    postLogToServer( path,completed: { _succ in
                        " ".WriteLineToURLNorm(path);
                       completed?(successData : _succ);
                    })
                        
                        
                    
                        
                            //NSLog(Read(path))
                    }
        
        
    }
    
    public class func postLogToServer(path : NSURL,
                                      completed: ((success : Bool) -> Void)
                                      ){
        
        //Network Request
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            // do your task
            
            dispatch_async(dispatch_get_main_queue()) {
        let loggedInModel = UserModel.GetInfo();
        
        if(loggedInModel != nil){
            if(loggedInModel.Authorization != nil){
        let device:UIDevice = UIDevice.currentDevice();
        let AppID = NSBundle.mainBundle().VersionName! as String;
        let AppVersion = NSBundle.mainBundle().buildVersionNumber!;
        let DeviceID :  String  = device.identifierForVendor!.UUIDString as String;
        let DeviceName :  String  = UIDevice.currentDevice().name as String;
        let email: String = loggedInModel.email_address! as String;
        //let phoneNumber: String = loggedInModel.userInfo.phoneNumber! as String;
        let model: String = UIDevice().model as String;
        let AccountID: String = loggedInModel.Authorization! as String;
        let language = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode)!;
                
       
                var data = Read(path)
        
                data = "\n Version : " + AppVersion + data;
                data = "\n Pack : " + AppID	 + data;
                data = "\n Device UDID : " + DeviceID + data;
                data = "\n Device Name : " + DeviceName + data;
                data = "\n Email : " + email + data;
                data = "\n model : " + model + data;
                data = "\n AccountID : " + AccountID + data;
                data = "\n language : " + (language as! String) + data + "\n" ;
                
        
                
                let LogData = (data);
       
      

        
        // Initializing Items
        var postdata = [
            "platform":"ios",
            "package_name":"\(AppID)",
            "package_version":"\(AppVersion)",
           
            "phone_model":"\(model)",
            "android_version":"\(DeviceID)",
            "imei":"\(DeviceID)",
            "email":"\(email)",
            "carrier":"T%26T",
            "mccmnc":"\(310410)",
            "netiso":"us",
            "acctid":"\(AccountID)",
            "locale":"en_US",
            "log":"\(LogData)"
            ];
  
        let Url = LocationURL  + "/ioslog.php"
        //?package_name=\(AppID)&package_version=\(AppVersion)&phone_number=\(phoneNumber)&phone_model=\(phone_model)&android_version=\(DeviceID)&imei=\(DeviceID)&email=\(email)&carrier=AT%26T&mccmnc=310410&netiso=us&acctid=\(AccountID)&locale=en_US&log=\(LogData)/"
                NetworkHelper.GeneralRequest("POST", Url: Url, postData: postdata ,success: { (successData) in
                    WritingLog = false;
                    NSLog("success");
                    completed(success:true);
                    }, failure: { (error) in
                        NSLog("error");
                        WritingLog = false;
                         completed(success:false);
                })
            }
        }
     }
        }
    }
    
    

}

extension NSData {
    func appendToURL(fileURL: NSURL) throws {
        if let fileHandle = try? NSFileHandle(forWritingToURL: fileURL) {
            defer {
                fileHandle.closeFile()
            }
            fileHandle.seekToEndOfFile()
            fileHandle.writeData(self)
        }
        else {
            try writeToURL(fileURL, options: .DataWritingAtomic)
        }
    }
}

extension String {
    func appendLineToURL(fileURL: NSURL) throws {
        try self.stringByAppendingString("\n").appendToURL(fileURL)
    }
    func WriteLineToURL(fileURL: NSURL) throws {
        let data = self.dataUsingEncoding(NSUTF8StringEncoding)!
        data.writeToURL(fileURL, atomically: true)
    }
    func WriteLineToURLNorm(fileURL: NSURL) {
        let data = self.dataUsingEncoding(NSUTF8StringEncoding)!
        data.writeToURL(fileURL, atomically: true)
    }
    func appendToURL(fileURL: NSURL) throws {
        let data = self.dataUsingEncoding(NSUTF8StringEncoding)!
        try data.appendToURL(fileURL)
    }
}
