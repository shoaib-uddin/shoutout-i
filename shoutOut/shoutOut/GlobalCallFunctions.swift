//
//  GlobalCallFunctions.swift
//  shoutOut
//
//  Created by clines193 on 7/18/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit



class ShoutoutAPI {
    
   
    //
    // Global Network Functions
    
    class func UpdatePassword(previous_password: String, new_password: String, completion: (callback: Bool) -> Void ){
        
        // Post Model Create
        let postModel =
            [
                "password" : "\(previous_password)",
                "new_password" : "\(new_password)"
        ]
        
        //Network Request
        NetworkHelper.MakePostRequest("shoutout_change_password", postData: postModel, showLoader: true, success: { (successData) -> Void in
                completion(callback: true);
            },failure: { (error) -> Void in
                completion(callback: false);
                
        })

    
    }
    
    
    class func UpdateUserInfo(first_name: String, last_name: String, email_address: String, image_string: String, completion: (callback: Bool) -> Void   ){
        
        let loc = currLocation;
        
        let postModel =
            [
                "first_name" : "\(first_name)",
                "last_name" : "\(last_name)",
                "email_address" : "\(email_address)",
                "image" : "\(image_string)",
                "latitude" : "\(loc.coordinate.latitude)",
                "longitude" : "\(loc.coordinate.longitude)",
                "altitude" : "\(loc.altitude)",
                "location_flag" : 1,
                ]
        
        //Network Request
        NetworkHelper.MakePostRequest("shoutout_update_user_info", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
            
                let info = UserModel.GetInfo();
                let auth = info.Authorization;
                let response : LoggedInModel = LoggedInModel(dictionary: successData as! NSDictionary);
                response.Authorization = auth;
                UserModel.setLoginUser(response);
                completion(callback: true);
            
            },failure: { (error) -> Void in
                completion(callback: false);
                
        })
    
    }
    
    
    class func RegisterUser(SignupFirstName: String, SignupLastName: String, SignupEmail: String, SignupPassword: String, UserImage: String, UserTag: String, deviceTokenString: String, deviceLat: Double, deviceLong: Double, deviceAltitude: Double,  completion: (callback: Bool) -> Void){
        
        
        
        // Post Model Create
        let postModel =
            [
                "first_name" : "\(SignupFirstName)",
                "last_name" : "\(SignupLastName)",
                "email_address" : "\(SignupEmail)",
                "password" : "\(SignupPassword)",
                "image" : "\(UserImage)",
                "user_tag" : "\(UserTag)",
                "device_token": deviceTokenString,
                "latitude" : deviceLat,
                "longitude" : deviceLong,
                "altitude" : deviceAltitude
        ]
        
        //Network Request
        NetworkHelper.MakePostRequest("shoutout_register", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
            let response : LoggedInModel = LoggedInModel(dictionary: successData as! NSDictionary);
            
            if(response.Authorization != nil){
                response.email_address = SignupEmail;
                UserModel.setLoginUser(response);
                
                completion(callback: true);
                
            }
            else{
                completion(callback: false);
            }
            
            
            },failure: { (error) -> Void in
                
                
        })
        
    }
    
    
    class func SendInvitationEmail(UserEMAIL: String, completion: (callback: Bool) -> Void){
        
        // Post Model Create
        let postModel = ["email_address" : UserEMAIL];
        
        //Network Request
        NetworkHelper.MakePostRequestForArray("shoutout_send_invitation_email", postData: postModel, showLoader: true, success: { (successData) -> Void in
                completion(callback: true);
                        
            },failure: { (error) -> Void in
                
                completion(callback: false);
                
            }
        )
    
    }

    
    
    
    class func ShoutoutLogin(UserName: String, Password: String, completion: (callback: Bool) -> Void){
    
        let deviceTokenString = UserModel.getKey("device_token");
        // Post Model Create
        let postModel = ["email_address":"\(UserName)", "password":"\(Password)", "device_token":"\(deviceTokenString)" ];
        
        //Network Request
        NetworkHelper.MakePostRequest("shoutout_login", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
            let response : LoggedInModel = LoggedInModel(dictionary: successData as! NSDictionary);
            
            if(response.Authorization != nil){
                response.email_address = UserName;
                UserModel.setLoginUser(response);
                //Network Request
                completion(callback: true);
                
            }
            else{
                completion(callback: false);
            }
            },failure: { (error) -> Void in
                var x = error
                
        })

    
    
    
    
    }
    
    
    class func ForgotPassword(UserEmail: String, completion: (isEmailSent: Bool) -> Void){
        
        // Post Model Create
        let postModel =
            [ "email":  "\(UserEmail)"
                
        ];
        
        
        //Network Request
        NetworkHelper.MakePostRequest("/ShoutOut_forgot_password", postData: postModel, showLoader: true, success: { (successData) -> Void in
                completion(isEmailSent: true)
            },failure: { (error) -> Void in
                completion(isEmailSent: false)
        })

        
    }
    
    class func DeleteCommentFromServer(id: String, completion: (callback: Bool) -> Void){
        
        
        NetworkHelper.MakeRequestForArray("DELETE", Url: "shoutout_delete_comment/" + "\(id)" , success: { (successData) in
                completion(callback: true)
            },failure: { (error) in
                completion(callback: false)
        });
        
       
        
    }
    
    class func DeleteShoutoutFromServer(id: String, completion: (callback: Bool) -> Void ){
        
        
        
        NetworkHelper.MakeRequestForArray("DELETE", Url: "shoutout_delete_user_feed/" + "\(id)" , success: { (successData) in
                completion(callback: true)
            },failure: { (error) in
                completion(callback: false)
        });
        
        
    }
    
    class func followFeedOfThisUser(following_id: String, isFollow: String, completion: (callback: Bool) -> Void ){
        
        // Post Model Create
        let postModel = ["following_id" : following_id,
                         "follow" : isFollow];
        
        //Network Request
        NetworkHelper.MakePostRequestForArray("shoutout_follow_user", postData: postModel, showLoader: true, success: { (successData) -> Void in
                completion(callback: true)
            },failure: { (error) -> Void in
                completion(callback: false)
                
        })
        
       
        
    }
    
    
    class func sendLIkeorDislikeToServer(feed_id: Int, dislike: Int, completion: (callback: Bool) -> Void ) {
        
        
        // Post Model Create
        let postModel = [ "feed_id" : feed_id, "dislike" : dislike ]
        
        //Network Request
        NetworkHelper.MakePostRequest("shoutout_add_like", postData: postModel, showLoader: false, success: { (successData) -> Void in
                completion(callback: true)
            },failure: { (error) -> Void in
                completion(callback: false)
        })
        
    }
    
    
    class func sendForgetPasswordEmail(UserEmail: String, completion: (isEmailSent: Bool) -> Void){
        
        // Post Model Create
        let postModel =
            [ "email_address" : "\(UserEmail)"]
        
        //Network Request
        NetworkHelper.MakePostRequestForArray("shoutout_forgot_password", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
            completion(isEmailSent: true)
            
            },failure: { (error) -> Void in
                completion(isEmailSent: false)
                
        })
        
        
        
    }
        
    class func checkIfEmailExists(UserEmail: String, completion: (isEmailExist: Bool) -> Void) {
        
        // Post Model Create
        let postModel =
            [ "email_address":  "\(UserEmail)"];
        
        var flag: Bool = false;
        
        //Network Request
        NetworkHelper.MakePostRequest("shoutout_check_email_exist", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
                let md = EmailModel(dictionary: successData as! NSDictionary);
                completion(isEmailExist: md.email_exist)
            
            },failure: { (error) -> Void in
                completion(isEmailExist: flag)
                
        })
        
        // invoke the completion callback
        // pass along the completed sketch animation instance
        
    }
}






