//
//  AppDelegate.swift
//  shoutOut
//
//  Created by Fez on 6/7/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import INTULocationManager

var currLocation : CLLocation!;


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var previousSelectedTabIndex: Int?
    var locMgr: INTULocationManager = INTULocationManager.sharedInstance();
    var requestLocationid  : INTULocationRequestID!;
    
    
    


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        subscribeToLocation()
        IQKeyboardManager.sharedManager().enable = true;
        registerForPushNotifications(application);
        
        
        // Facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    
    
    
    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    
    
    private func subscribeToLocation(){

        
        locMgr.subscribeToLocationUpdatesWithDesiredAccuracy(INTULocationAccuracy.City) { (loc, Accurany, _Status) in
             currLocation = loc;
        }
        
        
    }
    
    
    

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
        // facebook call
        FBSDKAppEvents.activateApp()
        
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        
        let deviceTokenString: String = deviceToken.description.stringByReplacingOccurrencesOfString("<", withString: "").stringByReplacingOccurrencesOfString(">", withString: "").stringByReplacingOccurrencesOfString(" ", withString: "")
        let defaults = NSUserDefaults.standardUserDefaults()
        
        print(deviceTokenString);
        UserModel.setKey("device_token", KeyValue: deviceTokenString);
        defaults.setObject(deviceTokenString, forKey: "Device_Token")
    }
    
    
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Device token for push notifications: FAIL -- ")
        print(error.description)
    }
    
    func registerForPushNotifications(application: UIApplication) {
        let notificationSettings = UIUserNotificationSettings(
            forTypes: [.Badge, .Sound, .Alert], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
        application.registerForRemoteNotifications();
    }
    
    
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        
        let  state : UIApplicationState = UIApplication.sharedApplication().applicationState;
        if (state == UIApplicationState.Background || state == UIApplicationState.Inactive)
        {
            //Do checking here.
            
            
            
            let temp : NSDictionary = userInfo
            
            let data : NotificationObject = NotificationObject(dictionary: temp);
            
            if(data.aps != nil){
                
               // NotificationHelper.NavigateToPushView( (data.extra_infor?.type)!, RefrenceID: (data.extra_infor?.reference_id)!,RefrenceName: "" ,_self:UIApplication.topViewController()!)
               // ;
                
            }
        }
        
        
        
    }
        
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
   
    
    


}

