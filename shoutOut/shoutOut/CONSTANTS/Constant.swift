//
//  Constant.swift
//  
//
//  Created by Fez on 3/21/16.
//
//

import Foundation
import UIKit

struct  DateFormatter {
    static let timeString = "hh:mm a"
    static let fullDate = "dd-MMM-yyyy HH:mm:ss.SSS";
    static let postDate = "yyyy-MM-dd HH:mm:ss";
    static let minuteSeconds = "HH:mm";
    static let WeekText = "EEEE";
    static let Date = "dd-MMM-yyyy";
}

struct settings
{
    static let timeOut :Double = 5.0;
    
struct KeyNames
{
    static let keyLocationSync = "SYNC_SNAP_TO_OTHER_DEVICES";
    static let distance = "DISTANCE";
}

struct DistanceText{
    static let feet :String = "Feet";
    static let meter :String = "Meters";
     static let meter_one :String = "Meter";
     static let foot_one :String = "Foot";
}

struct Notificationkeys{
    static let LocationIdentifier = "LocationNotification"
    static let InitIdentifier = "ShoutOutIdentifier"
}
 
    struct messages{
        static let GPSNotAvaialable = " Your GPS appears to be disabled "
        static let InternetNotAvailable = " Your Internet Connection appears to be disabled or very Slow "
        static let LimitedInternetConnection = "There is no Data Connected"
        static let AirplaneMode = "It Appears, your Phone is in Airplane Mode"
        static let invalidCredentialsText = "account_id or session_id, Invalid value"
        static let  sessionExpired = "your session has expired"
        static let  timeOutErrorMesasge = "The request timed out.";
        static let  errorCode = -1001;
    }
    
    
}


struct ScreenKeys
{
     static let Home = "pref_Home";
     static let AddSnap = "pref_AddSnap";
     static let SnapLocation = "pref_SnapLocation";
     static let ShowSnapLocationhHeader = "pref_ShowSnapLocationhHeader";
    
    
}


struct limits{
    static let distance : Double = 1.99 ;
     static let seconds : Int = 1 ;
}

struct General{
    static let EmergencyNumber : String = "511" ;
    static var CountDownTimer : Int = 5;
    static let htmlFontSize : CGFloat = 13;
    static let NormalTimer : Int = 5;
    static let TouchMenuSpecialTimer : Int = 4;
    
    static let logFileName : String = "shoutoutConsole.log" ;
    
}

struct credentials{
    static let api_key = "f7c3bc1d808e04732adf679965ccc34ca7ae3441"
    static let api_secret_key = "d13e8a3d2ae7d015a6f439c72f799838c3db8d1a"
}




//Global Variables:

var ParamHorizontalAccuracy : Double = 0;
var pendingRequest: Bool = false;
var LastLocation : GeoLocations!;
var NavigatetoAddLocation : Bool = false;
var inCall:Bool = false;
var ErrorinConnection = false;
var directlyInitializeCall: Bool = false;

func delay(delay:Double, closure:()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(), closure)
}

enum AlertType{
    case InternetNotAvailable
    case GPSNotAvailable
    case CallNotAvailable
    case LimitedConnectivityAvailable
}

func get_States() -> Array<States>{
    
    var _states :  Array<States>  = Array<States>();
    _states.append(States(StateName: "Alabama", StatePrefx: "AL"));
    _states.append(States(StateName: "Alaska", StatePrefx: "AK"));
    _states.append(States(StateName: "Arizona", StatePrefx: "AZ"));
    _states.append(States(StateName: "Arkansas", StatePrefx: "AR"));
    _states.append(States(StateName: "California", StatePrefx: "CA"));
    _states.append(States(StateName: "Colorado", StatePrefx: "CO"));
    _states.append(States(StateName: "Connecticut", StatePrefx: "CT"));
    _states.append(States(StateName: "Delaware", StatePrefx: "DE"));
    _states.append(States(StateName: "District Of Columbia", StatePrefx: "DC"));
    _states.append(States(StateName: "Florida", StatePrefx: "FL"));
    _states.append(States(StateName: "Georgia", StatePrefx: "GA"));
    _states.append(States(StateName: "Hawaii", StatePrefx: "HI"));
    _states.append(States(StateName: "Idaho", StatePrefx: "ID"));
    _states.append(States(StateName: "Illinois", StatePrefx: "IL"));
    _states.append(States(StateName: "Indiana", StatePrefx: "IN"));
    _states.append(States(StateName: "Iowa", StatePrefx: "IA"));
    _states.append(States(StateName: "Kansas", StatePrefx: "KS"));
    _states.append(States(StateName: "Kentucky", StatePrefx: "KY"));
    _states.append(States(StateName: "Louisiana", StatePrefx: "LA"));
    _states.append(States(StateName: "Maine", StatePrefx: "ME"));
    _states.append(States(StateName: "Maryland", StatePrefx: "MD"));
    _states.append(States(StateName: "Massachusetts", StatePrefx: "MA"));
    _states.append(States(StateName: "Michigan", StatePrefx: "MI"));
    _states.append(States(StateName: "Minnesota", StatePrefx: "MN"));
    _states.append(States(StateName: "Mississippi", StatePrefx: "MS"));
    _states.append(States(StateName: "Missouri", StatePrefx: "MO"));
    _states.append(States(StateName: "Montana", StatePrefx: "MT"));
    _states.append(States(StateName: "Nebraska", StatePrefx: "NE"));
    _states.append(States(StateName: "Nevada", StatePrefx: "NV"));
    _states.append(States(StateName: "New Hampshire", StatePrefx: "NH"));
    _states.append(States(StateName: "New Jersey", StatePrefx: "NJ"));
    _states.append(States(StateName: "New Mexico", StatePrefx: "NM"));
    _states.append(States(StateName: "New York", StatePrefx: "NY"));
    _states.append(States(StateName: "North Carolina", StatePrefx: "NC"));
    _states.append(States(StateName: "North Dakota", StatePrefx: "ND"));
    _states.append(States(StateName: "Ohio", StatePrefx: "OH"));
    _states.append(States(StateName: "Oklahoma", StatePrefx: "OK"));
    _states.append(States(StateName: "Oregon", StatePrefx: "OR"));
    _states.append(States(StateName: "Pennsylvania", StatePrefx: "PA"));
    _states.append(States(StateName: "Rhode Island", StatePrefx: "RI"));
    _states.append(States(StateName: "South Carolina", StatePrefx: "SC"));
    _states.append(States(StateName: "South Dakota", StatePrefx: "SD"));
    _states.append(States(StateName: "Tennessee", StatePrefx: "TN"));
    _states.append(States(StateName: "Texas", StatePrefx: "TX"));
    _states.append(States(StateName: "Utah", StatePrefx: "UT"));
    _states.append(States(StateName: "Vermont", StatePrefx: "VT"));
    _states.append(States(StateName: "Virginia", StatePrefx: "VA"));
    _states.append(States(StateName: "Washington", StatePrefx: "WA"));
    _states.append(States(StateName: "West Virginia", StatePrefx: "WV"));
    _states.append(States(StateName: "Wisconsin", StatePrefx: "WI"));
    _states.append(States(StateName: "Wyoming", StatePrefx: "WY"));
    return _states;

}

