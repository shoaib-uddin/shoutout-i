//
//  CSSLayouts.swift
//  ShoutOut
//
//  Created by clines193 on 6/4/16.
//  Copyright © 2016 matech. All rights reserved.
//

import Foundation
import UIKit

func setUITextViewCons(v: UITextField, icon: String, showIcon: Bool){
    
    
    // border and border color
    v.layer.cornerRadius = 15.0;
    v.layer.masksToBounds = true;
    v.layer.borderColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.5).CGColor;
    v.layer.borderWidth = 1.0;
    
    // height of text field
    v.frame.size.height = 70;
    
    if(showIcon){
        
        
        // add image and indent a little
        let imgLeft: UIImageView = UIImageView(frame: CGRectMake(0, 0, v.frame.height-25, 40 ))
        // Set frame as per space required around icon
        imgLeft.image = UIImage(named: icon)
        imgLeft.contentMode = .Center
        // Set content mode centre
        v.leftView = imgLeft;
    
        // add a right border on image
        let sublayer: CALayer = CALayer();
        sublayer.backgroundColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.5).CGColor;

        sublayer.frame = CGRectMake(imgLeft.bounds.size.width, 0, 1, imgLeft.frame.size.height)
        imgLeft.layer.addSublayer(sublayer)
        v.leftViewMode = .Always
        
        
 
    }else{
        let paddingView = UIView(frame: CGRectMake(0, 0, 20, v.frame.height))
        v.leftView = paddingView
        v.leftViewMode = UITextFieldViewMode.Always
    
    
    
    
    
    }
}





func setUIButtonCons(v: UIButton){
    // border and border color
    v.layer.cornerRadius = 10.0;
    v.layer.masksToBounds = true;
    v.layer.borderColor = UIColor.whiteColor().CGColor;
    v.layer.borderWidth = 1.0;

}

func setBottomBorder(v: UIButton){
    //Bottom border
    let bottomBorder: CALayer = CALayer();
    bottomBorder.frame = CGRectMake(0.0, v.superview!.frame.size.width , v.superview!.frame.size.width, 1.0)
    bottomBorder.backgroundColor = UIColor.whiteColor().CGColor
    v.layer.addSublayer(bottomBorder)
    
}

func setBottomBorder(v: UILabel){
    //Bottom border
    let bottomBorder: CALayer = CALayer();
    bottomBorder.frame = CGRectMake(0.0, v.frame.height, (v.superview?.layer.frame.size.width)!, 1.0)
    bottomBorder.backgroundColor = UIColor.whiteColor().CGColor
    v.layer.addSublayer(bottomBorder)
    
}

func setBottomBorderGreen(v: UILabel){
    //Bottom border
    let bottomBorder: CALayer = CALayer();
    bottomBorder.frame = CGRectMake(0.0, v.frame.height, (v.superview?.layer.frame.size.width)!, 1.0)
    bottomBorder.backgroundColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 1).CGColor;
    v.layer.addSublayer(bottomBorder)
    
}





func transInnerWhiteOuter(v: UIView){
    v.layer.cornerRadius = v.frame.size.height/2;
    v.clipsToBounds = true;
    v.layer.borderWidth = 1.0;
    v.layer.borderColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 1).CGColor
    
    v.layer.backgroundColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.2).CGColor
    
}

func whiteBackgroundCircularImage(v: UIImageView){
    v.layer.cornerRadius = v.frame.size.height/2;
    v.clipsToBounds = true;
    v.layer.backgroundColor = UIColor.whiteColor().CGColor;
    
    
}

func setUIButtonImageWText(v: UIButton, text: String, img: String){
    let teamImage: UIButton = UIButton(frame: CGRect(x: 0, y: 75, width: 100, height: 50))
    let imageTest = UIImage(named: img)
    
    teamImage.setTitle(text, forState: .Normal)
    teamImage.setBackgroundImage(imageTest, forState: .Normal)
    v.addSubview(teamImage)

}


func fontawesomeIcon(v: UIButton, icon: String){
    
    /*var faicon = [String: UniChar]();
    faicon["famusic"] = 0xf001;
    
    
    var label: UILabel = UILabel(frame: CGRectMake(0, CGFloat(20), 20, 20));
    label.font = UIFont(name: "FontAwesome", size: 40);
    label.text = String(format: "%C", faicon["famusic"]!);
    
    label.textColor = UIColor.whiteColor()
    //v.leftView = label
    //v.leftViewMode = .Always
    */


}






func heartFontawesomeIconButton(v: UIButton, icon: String, selected: Bool, text: String){
    
    if(!selected){
        v.selectedButton(text + " likes  ", iconName: icon);
    }else{
        v.selectedButton(text + " likes  ", iconName: icon);
    }
    
}

func commentFontawesomeIconButton(v: UIButton, icon: String, selected: Bool, text: String){
    
    if(!selected){
        v.selectedButton(text + " comments  ", iconName: icon);
    }else{
        v.selectedButton(text + " comments  ", iconName: icon);
    }

    
}

func followFontawesomeIconButton(v: UIButton, icon: String, selected: Bool, text: String){
    
    if(!selected){
        v.selectedButton(text + "", iconName: icon);
    }else{
        v.selectedButton(text + "", iconName: icon);
    }
    
    
}



func setPositionElementsInButton(button: UIButton){
    
    button.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    button.titleLabel!.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    button.imageView!.transform = CGAffineTransformMakeScale(-1.0, 1.0);


}








func setHalfRightBorder(v: UIButton){
    v.clipsToBounds = false;
    let rightBorder: CALayer = CALayer();
    rightBorder.borderColor = UIColor.whiteColor().CGColor
    rightBorder.borderWidth = 1
    rightBorder.frame = CGRectMake(v.frame.maxX + 2, 6.0,1.0, v.frame.size.height - 12.0);
    v.layer.addSublayer(rightBorder)
}

func setBtnleftBorder(v: UIButton){
    v.clipsToBounds = true;
    let leftBorder: CALayer = CALayer();
    leftBorder.borderColor = UIColor.whiteColor().CGColor
    leftBorder.borderWidth = 1
    leftBorder.frame = CGRectMake(0, 0.0,1.0, (v.superview?.frame.size.height)!);
    v.layer.addSublayer(leftBorder)

}



func SetFatBottomBorderToZero(v: UIButton){
    
}


let FatBottomBorder: CALayer = CALayer();

func setInitialFatBottomBorder(v: UIButton){
    
    v.clipsToBounds = true;
    FatBottomBorder.borderColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8).CGColor
    FatBottomBorder.borderWidth = 4
    //FatBottomBorder.frame = CGRectMake(0.0, v.frame.size.height - 10,v.frame.size.width, 6.0);
    //FatBottomBorder.frame = CGRectMake(0.0, v.frame.size.height - 6,v.frame.size.width - 80, 6.0);
    v.layer.addSublayer(FatBottomBorder);
    
}


func setFatBottomBorder(v: UIButton, v2: UIButton, setlayer: Int){
    
    FatBottomBorder.removeFromSuperlayer();
    
    v.clipsToBounds = true;
    v2.clipsToBounds = true;
    FatBottomBorder.borderColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8).CGColor
    FatBottomBorder.borderWidth = 4
    
    if(setlayer == 1){
        
        print("run");
        FatBottomBorder.frame = CGRectMake(40.0, v.frame.size.height - 6,v.frame.size.width - 80, 6.0);
        
        
        v.layer.addSublayer(FatBottomBorder);
        
    }else{
        print("false");
        FatBottomBorder.frame = CGRectMake(40.0, v.frame.size.height - 6,v2.frame.size.width - 80, 6.0);
        v2.layer.addSublayer(FatBottomBorder);

        
    }
    
    
    
    
}

func spLabelTopTable(v: UILabel){
    
    //Bottom border
    let bottomBorder: CALayer = CALayer();
    bottomBorder.frame = CGRectMake(-30.0, v.frame.height, (v.superview?.layer.frame.size.width)! - 50, 1.0)
    bottomBorder.backgroundColor = UIColor.blackColor().CGColor
    v.layer.addSublayer(bottomBorder)
    
    
}

func bottomBorderLabelUserTagTable(v: UILabel){
    
    //Bottom border
    let bottomBorder: CALayer = CALayer();
    bottomBorder.frame = CGRectMake(-20.0, v.frame.height, (v.superview?.layer.frame.size.width)! , 1.0)
    bottomBorder.backgroundColor = UIColor(red: 0 / 255.0, green: 178 / 255.0, blue: 148 / 255.0, alpha: 0.8).CGColor
    
    v.layer.addSublayer(bottomBorder)
    
    
}
func evenUserTagLabelColor(v: UILabel){
    
    v.textColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8);
    //v.textColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 1);
}

func addTextLayerOnImageCenter(v: UILabel){
    let string = "+ \nUPLOAD \nPHOTO";
    v.text = string;
}


func roundGreenImageBorder(v: UIImageView){
    
    v.layer.cornerRadius = v.frame.size.height/2;
    v.clipsToBounds = true;
    v.layer.borderColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 1).CGColor;
    v.layer.borderWidth = 2;

}

func roundWhiteImageBorder(v: UIImageView){
    
    v.layer.cornerRadius = v.frame.size.height/2;
    v.clipsToBounds = true;
    v.layer.borderColor = UIColor.whiteColor().CGColor;
    v.layer.borderWidth = 2;
    
}

func roundWhiteImageBorderWidthWise(v: UIImageView){
    
    v.layer.cornerRadius = v.frame.size.width/2;
    v.clipsToBounds = true;
    v.layer.borderColor = UIColor.whiteColor().CGColor;
    v.layer.borderWidth = 2;
    
}



func allSideWhiteBorder(v: UIButton){
    v.layer.borderWidth = 1;
    v.layer.borderColor = UIColor.whiteColor().CGColor;
    v.layer.cornerRadius = 8;
}

func allSideWhiteBorder(v: UITextView){
    
    var  frame : CGRect = v.frame;
    frame.size.height = v.contentSize.height;
    v.frame = frame;
    
    

}

func smallAlternateButtons(v: UIButton, color: String){
    
    v.layer.borderWidth = 1;
    v.layer.cornerRadius = v.layer.frame.height / 2.0;
    
    
    if(color == "green"){
        v.backgroundColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8);
        v.layer.borderColor = UIColor.whiteColor().CGColor;
    }
    else{
        v.backgroundColor = UIColor.clearColor();
        v.layer.borderColor = UIColor.whiteColor().CGColor;
        
        
    }
    
}

func allFeedWhiteButtons(v: UIButton){
    
    v.backgroundColor = UIColor.whiteColor();
    


}


func smallWhiteAlternateButton(v: UILabel, color: String){
    
    v.layer.borderWidth = 1;
    v.layer.cornerRadius = v.layer.frame.height / 2.0;
    
    
    if(color == "green"){
        v.backgroundColor = UIColor.whiteColor();
        v.layer.borderColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8).CGColor;
        v.textColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8);
    }
    else{
        v.backgroundColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8);
        v.layer.borderColor = UIColor.whiteColor().CGColor;
        v.textColor = UIColor.whiteColor();
        
        
    }
    
}

func smallWhiteAlternateButton(v: UIButton, color: String){
    
    v.layer.borderWidth = 1;
    v.layer.cornerRadius = v.layer.frame.height / 2.0;
    
    
    if(color == "green"){
        v.backgroundColor = UIColor.whiteColor();
        v.layer.borderColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8).CGColor;
        v.setTitleColor(UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8), forState: .Normal)
    }
    else{
        v.backgroundColor = UIColor(red: 21 / 255.0, green: 93 / 255.0, blue: 105 / 255.0, alpha: 0.8);
        v.layer.borderColor = UIColor.whiteColor().CGColor;
        v.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        
    }
    
}


func customizeToggleButton(ToggleFeedMapView: UISegmentedControl){
    
    ToggleFeedMapView.backgroundColor = UIColor.clearColor()
    if ToggleFeedMapView.selectedSegmentIndex == 0 {
        UIView.animateWithDuration(0.5, animations: {
            
            
            
            
        })
    } else {
        UIView.animateWithDuration(0.5, animations: {
            
            
            
        })
    }
}



extension UIButton {
    func selectedButton(title:String, iconName: String){
        
        let myNormalAttributedTitle = NSAttributedString(string: title,
                                                         attributes: [NSFontAttributeName: UIFont.systemFontOfSize(11.0)]);
        
        self.setAttributedTitle(myNormalAttributedTitle, forState: .Normal);
        self.setImage(UIImage(named: iconName), forState: UIControlState.Normal)
        self.layoutIfNeeded()
    }
}



// Add anywhere in your app
extension UIImage {
    func imageWithColor(tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        
        let context = UIGraphicsGetCurrentContext()! as CGContextRef
        CGContextTranslateCTM(context, 0, self.size.height)
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextSetBlendMode(context, CGBlendMode.Normal)
        
        let rect = CGRectMake(0, 0, self.size.width, self.size.height) as CGRect
        CGContextClipToMask(context, rect, self.CGImage)
        tintColor.setFill()
        CGContextFillRect(context, rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}






