//
//  ViewProfileViewController.swift
//  shoutOut
//
//  Created by clines193 on 6/14/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

class ViewProfileViewController: BaseViewController{
    
    @IBOutlet weak var OuterImageView: UIView!
    @IBOutlet weak var imgProfilePicture: UIImageView!
    @IBOutlet weak var lblUserTag: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblProfileID: UILabel!
    @IBOutlet weak var lblEmailProfile: UILabel!
    @IBOutlet weak var tbxEditProfileImageButton: UIButton!
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var btnLogoutUser: UIButton!
    @IBOutlet weak var btnMyShoutout: UIButton!
    
    var info: LoggedInModel = UserModel.GetInfo();
    var UserImage: String = "";
    
    let recognizer = UITapGestureRecognizer();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        // CSSLayout File
        transInnerWhiteOuter(OuterImageView);
        whiteBackgroundCircularImage(imgProfilePicture);
        setUIButtonCons(btnMyShoutout);
        setUIButtonCons(btnEditProfile);
        
        // image TAP
        imgProfilePicture.userInteractionEnabled = true
        recognizer.addTarget(self, action: #selector(self.plusBtnTapped))
        imgProfilePicture.addGestureRecognizer(recognizer)
        
        
        
        
        // hide tab bar for this view
        self.tabBarController?.tabBar.hidden = true
        
    }
    
    
    func plusBtnTapped(sender: UITapGestureRecognizer) {
       LazyImage.zoomImageView(imgProfilePicture)
    }

    
    
    
    
    override func viewDidAppear(animated: Bool) {
        // hide tab bar for this view
        self.tabBarController?.tabBar.hidden = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        info = UserModel.GetInfo();
        // to reload selected cell
        
        // set info in profile
        lblUsername.text = (info.first_name! + " " + info.last_name!).uppercaseString;
        lblUserTag.text = "(@"+info.user_tag!+")";
        lblEmailProfile.text = info.email_address;
        lblProfileID.text = "Password: ********* ";
        LazyImage.showForImageView(imgProfilePicture, url: SERVICEURL+info.image!)
    }
    
   
    
    @IBAction func btnTopSignout(sender: UIBarButtonItem) {
        UserModel.setLoggedOutUser();
        let loginVC: UIViewController? = self.storyboard?.instantiateViewControllerWithIdentifier("SplashViewController")
        self.presentViewController(loginVC!, animated: true, completion: nil)
    }
    
    @IBAction func BackButtonPressed(sender: UIBarButtonItem) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        print(appDelegate.previousSelectedTabIndex);
        let tabBarController = self.tabBarController!
        tabBarController.selectedIndex = appDelegate.previousSelectedTabIndex!
    }
    
    
    @IBAction func btnGotoMyShoutouts(sender: UIButton) {
        gotoMyshoutouts();
    }
    
    
    func gotoMyshoutouts(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let destination = storyboard.instantiateViewControllerWithIdentifier("MyShoutoutViewController") as! MyShoutoutViewController
        self.navigationController?.pushViewController(destination, animated: true)
        
    }

    
    @IBAction func btnGotoEditProfile(sender: UIButton) {
        gotoEditProfileView()
    }
        
    func gotoEditProfileView() {
        
        // Initializing Items
        let UserFirstName: String = info.first_name!  as String;
        let UserLastName: String = info.last_name!  as String;
        let EmailAddress: String = info.email_address!  as String;
        let Tag: String = info.user_tag! as String;
        let theImage: String = SERVICEURL+info.image! as String;
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let destination = storyboard.instantiateViewControllerWithIdentifier("EditProfileViewController") as! EditProfileViewController
        
        destination.UserFirstName = UserFirstName;
        destination.UserLastName = UserLastName;
        destination.UserEmail = EmailAddress;
        destination.UserImage = theImage;
        destination.UserTag = Tag;
        
        self.navigationController?.pushViewController(destination, animated: true);
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
