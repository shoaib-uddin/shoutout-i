//
//  ChangePasswordViewController.swift
//  shoutOut
//
//  Created by clines193 on 7/28/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit

class ChangePasswordViewController: BaseViewController,
UINavigationControllerDelegate, UITextFieldDelegate {
    
    var info: LoggedInModel = UserModel.GetInfo();
    
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var OuterImageView: UIView!
    @IBOutlet weak var lblUserTag: UILabel!
    @IBOutlet weak var tbxUserPreviousPassword: UITextField!
    @IBOutlet weak var tbxUserPassword: UITextField!
    @IBOutlet weak var tbxConfirmPassword: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    
    var UserPassword: String = "";
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LazyImage.showForImageView(imgUser, url: SERVICEURL+info.image!)
        // Do any additional setup after loading the view.
        lblUserTag.text = UserModel.GetInfo().user_tag;
        
        // CSSLayout File
        // CSSLayout File
        transInnerWhiteOuter(OuterImageView);
        whiteBackgroundCircularImage(imgUser);
                
        setUIButtonCons(btnSave);
        setUITextViewCons(tbxUserPreviousPassword, icon: "a", showIcon: false);
        setUITextViewCons(tbxUserPassword, icon: "a", showIcon: false);
        setUITextViewCons(tbxConfirmPassword, icon: "a", showIcon: false);
        
        // set info values in tbx's
        lblUserTag.text = "(@"+info.user_tag!+")";
        
        // disable the new password field untill previous password is filled
        // Setting the Delegate for the TextField
        tbxUserPreviousPassword.delegate = self;
        tbxUserPassword.delegate = self;
        
        //textFieldDidEndEditing(tbxUserPreviousPassword);
        
        
        
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        if (tbxUserPreviousPassword.text != ""){
            self.tbxUserPassword.placeholder = "New Password"
            self.tbxUserPassword.enabled = true
        }else{
            self.tbxUserPassword.placeholder = "fill previous password first"
            self.tbxUserPassword.enabled = false
        }
    }
    
    
    
    
    
    
    func gotoViewProfileView() {
        
        UtilityHelper.NavigateToViewController(self, viewControllerid: "ViewProfileViewController")
        
    }
    
    
    
    
    
    
    @IBAction func btnSaveProfile(sender: UIButton) {
        UpdateUserProfileDetailToServer()
    }
    
    
    func UpdateUserProfileDetailToServer(){
        // Initializing Items
        let PrePassword: String = tbxUserPreviousPassword.text! as String;
        var Password: String = tbxUserPassword.text! as String;
        //doing Validations
        
        if(!Validate.MaxLength(Password, length: 10, Label: "Password")){return;}
        if(PrePassword != ""){
            
            if(!Validate.Required(Password, Label: "Password")){return;}
            if(!Validate.MatchStrings("\(tbxUserPassword.text!)", Text2: "\(tbxConfirmPassword.text!)", Label1: "Password", Label2: "confirm password")){return;}
            
            
        }
        
        if(!Validate.Required(PrePassword, Label: "Previous Password")){return;}
        
        ShoutoutAPI.UpdatePassword(PrePassword, new_password: Password) { (callback) in
            //
            if(callback){
                UtilityHelper.AlertMessagewithCallBack("Updated Successfully", success: {
                    self.navigationController?.popViewControllerAnimated(false)
                    
                })
            }
            
        }
        
        
        
        
        
        
        
        
    }
    
    
    
    override func viewDidAppear(animated: Bool) {
        // hide tab bar for this view
        self.tabBarController?.tabBar.hidden = true
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
