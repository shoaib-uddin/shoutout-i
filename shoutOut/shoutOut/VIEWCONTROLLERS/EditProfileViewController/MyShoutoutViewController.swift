//
//  MyShoutoutViewController.swift
//  shoutOut
//
//  Created by clines193 on 6/28/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit


class MyShoutoutViewController: BaseViewController,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
UITableViewDataSource , UITableViewDelegate, FeedlistCellDelegate, FeedImagelistCellDelegate{
    
    var feedList : Array<FeedModel> = Array<FeedModel>();
    @IBOutlet weak var tableView: UITableView!
    
    
    
        
    /*
     *  functions to Delete a feed of table cell and send to server
     *
     **/
    
    
    func DeleteMyShoutout(cell: FeedTextTableViewCell, actionType: String) {
        //
        DeleteThisFeed(self.feedList[cell.indexPathRow].id!, index: cell.indexPathRow)
    }
    
    func DeleteMyImageShoutout(cell: FeedImageTableViewCell, actionType: String) {
        //
        DeleteThisFeed(self.feedList[cell.indexPathRow].id!, index: cell.indexPathRow)
        
    }
    
    func DeleteThisFeed(feed_id: String, index: Int){
        
        let alert = UIAlertController(title: "Delete Feed", message: "Are you sure?" , preferredStyle: UIAlertControllerStyle.Alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: { action in
            
            // do something like...
            ShoutoutAPI.DeleteShoutoutFromServer(feed_id, completion: {(callback) in
                self.feedList.removeAtIndex(index);
                self.tableView.reloadData();
            })
            
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { action in
            
            // do something like...
            print("cancel")
            
        }))
        
        // show the alert
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "My ShoutOuts";
        
        //tableView.registerNib(UINib(nibName: "MyShoutoutTableViewCell", bundle: nil), forCellReuseIdentifier: "MyShoutoutTableViewCell")
        tableView.registerNib(UINib(nibName: "FeedTextTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedTextTableViewCell")
        tableView.registerNib(UINib(nibName: "FeedImageTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedImageTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 400;
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.backgroundView?.backgroundColor = UIColor.clearColor();
        tableView.backgroundColor = UIColor.clearColor();
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        // call a function to get feeds
       
        get_all_user_feeds();
        
    }
    
    func get_all_user_feeds(){
        
        // Post Model Create
        let postModel = ["start_record" : "0", "end_record" : 100];
        
        //Network Request
        NetworkHelper.MakePostRequestForArray("shoutout_get_all_user_feeds", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
            self.feedList = [FeedModel](json: successData);
            self.tableView.reloadData();
            
            },failure: { (error) -> Void in
                _ = error
                
        })
        
    }
    
    
    /*
     *  functions to make user follow the feed of table cell
     *
     **/
    
    func followThisFeed(cell: FeedTextTableViewCell, actionType: String) {
        cell.setFeedFollow(self.feedList[cell.indexPathRow]);
    }
    
    func followThisImageFeed(cell: FeedImageTableViewCell, actionType: String) {
        cell.setFeedFollow(self.feedList[cell.indexPathRow]);
    }
    

    
    

    /*
     *  functions to like a feed of table cell and send to server
     *
     **/
    
    var isFeedLiked:Bool = false;
    
    func isFeedLiked(cell: FeedTextTableViewCell, actionType: String) {
        //cell.setFeedLiked(self.feedList[cell.indexPathRow])
    }
    
    func isFeedImageLiked(cell: FeedImageTableViewCell, actionType: String) {
        //cell.setFeedLiked(self.feedList[cell.indexPathRow])
    }
    
    
    /*
     *  functions to transfer user to comments section of table cell
     *
     **/
    
    func showComments(cell: FeedTextTableViewCell, actionType: String) {
        showSignleFeedComments(cell.FeedID!, index: cell.indexPathRow)
    }
    
    func showImageComments(cell: FeedImageTableViewCell, actionType: String){
        showSignleFeedComments(cell.FeedID!, index: cell.indexPathRow)
    }
    
    func showSignleFeedComments(feed_id: String, index: Int){
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let destination = storyboard.instantiateViewControllerWithIdentifier("CommentFeedViewController") as! CommentFeedViewController
        destination.feed_id = feed_id;
        destination.feed = feedList[index];
        self.navigationController?.pushViewController(destination, animated: true);
        
    }

    
    
    
    
    override func viewDidAppear(animated: Bool) {
        
        // CSSLatout called
        
    }
    
    
    //Delegation Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedList.count;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        showSignleFeedComments(feedList[indexPath.row].id!, index: indexPath.row);

        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(feedList[indexPath.row].image == ""){
            
            let cell = tableView.dequeueReusableCellWithIdentifier("FeedTextTableViewCell", forIndexPath: indexPath) as! FeedTextTableViewCell
            cell.indexPathRow = indexPath.row;
            cell.setData(feedList[indexPath.row], myshoutout: true);
            cell.cellDelegate = self;
            return  cell;
            
        }
        else{
            
            let cell = tableView.dequeueReusableCellWithIdentifier("FeedImageTableViewCell", forIndexPath: indexPath) as! FeedImageTableViewCell
            cell.indexPathRow = indexPath.row;
            cell.setData(feedList[indexPath.row], myshoutout: true);
            cell.cellDelegate = self;
            return  cell;
            
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    
    
    
    
    
}