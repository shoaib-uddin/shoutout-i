//
//  EditProfileViewController.swift
//  shoutOut
//
//  Created by Fez on 6/7/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var OuterImageView: UIView!
    @IBOutlet weak var imgProfilePicture: UIImageView!
    @IBOutlet weak var lblUserTag: UILabel!
    @IBOutlet weak var tbxEditProfileImageButton: UIButton!
    @IBOutlet weak var btnSaveProfile: UIButton!
    
    @IBOutlet weak var tbxUserFirstName: UITextField!
    @IBOutlet weak var tbxUserLastName: UITextField!
    @IBOutlet weak var tbxUserEmail: UITextField!
    
    
    // String values
    var pickedBase64Image: String = "";
    var UserFirstName: String = "";
    var UserLastName: String = "";
    var UserEmail: String = "";
    var UserImage: String = "";
    var UserTag: String = "";
    
    
    
    let recognizer = UITapGestureRecognizer();
    let imagePicker = UIImagePickerController();
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        // CSSLayout File
        transInnerWhiteOuter(OuterImageView);
        whiteBackgroundCircularImage(imgProfilePicture);
        setUIButtonCons(btnSaveProfile);
        setUIButtonCons(tbxEditProfileImageButton);
        setUITextViewCons(tbxUserFirstName, icon: "a", showIcon: false);
        setUITextViewCons(tbxUserLastName, icon: "a", showIcon: false);
        setUITextViewCons(tbxUserEmail, icon: "a", showIcon: false);
        //setBottomBorder(lblUserTag);
        
        
        // set info values in tbx's
        tbxUserFirstName.text = UserFirstName;
        tbxUserLastName.text = UserLastName;
        tbxUserEmail.text = UserEmail;
        lblUserTag.text = UserTag;
        
        LazyImage.showForImageView(imgProfilePicture, url: UserImage)
        
        // initialize image as base64
        pickedBase64Image = UtilityHelper.ImageToBase64String(imgProfilePicture.image!);
        
        // image TAP
        imgProfilePicture.userInteractionEnabled = true
        recognizer.addTarget(self, action: #selector(self.plusBtnTapped))
        imgProfilePicture.addGestureRecognizer(recognizer)
        
        // IMAGE DELEGATE
        imagePicker.delegate = self;
        
        
        // disable the new password field untill previous password is filled
        //Setting the Delegate for the TextField
        tbxUserFirstName.delegate = self;
        tbxUserLastName.delegate = self;
        tbxUserEmail.delegate = self;
        
        
    }
    
    
    func gotoViewProfileView() {
        UtilityHelper.NavigateToViewController(self, viewControllerid: "ViewProfileViewController")
    }

    
    
    
    func plusBtnTapped(sender: UITapGestureRecognizer) {
        
        let alert = UIAlertController(title: "Select Picture Option", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let libButton = UIAlertAction(title: "Select photo from library", style: UIAlertActionStyle.Default) {  (alert) -> Void in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.imagePicker.allowsEditing = true
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            let cameraButton = UIAlertAction(title: "Take a picture", style: UIAlertActionStyle.Default) {  (alert) -> Void in
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
                self.imagePicker.allowsEditing = true
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
            }
            alert.addAction(cameraButton)
        } else {
            print("Camera not available")
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {  (alert) -> Void in
            print("Cancel Pressed")
        }
        
        alert.addAction(libButton)
        alert.addAction(cancelButton)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    
    //Image Picker Delegates
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        if let pickedImage = editingInfo![UIImagePickerControllerEditedImage] as? UIImage {
            
            imgProfilePicture.image = image
            pickedBase64Image = UtilityHelper.ImageToBase64String(image);
            
        }
        dismissViewControllerAnimated(true, completion: nil);
        
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            //hide label after picture choosen
            //lblUploadPhoto.hidden = true;
            
            imgProfilePicture.image = pickedImage
            pickedBase64Image = UtilityHelper.ImageToBase64String(pickedImage);
            
            
            
            
        }
        dismissViewControllerAnimated(true, completion: nil);
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func btnSaveProfile(sender: UIButton) {
        UpdateUserProfileDetailToServer()
    }
    
    
    func UpdateUserProfileDetailToServer(){
        // Initializing Items
        let UserFirstName: String = tbxUserFirstName.text!  as String;
        let UserLastName: String = tbxUserLastName.text!  as String;
        let EmailAddress: String = tbxUserEmail.text!  as String;
        
        //doing Validations
        if(!Validate.Required(UserFirstName, Label: "First Name")){return;}
        if(!Validate.Required(UserLastName, Label: "Last Name")){return;}
        if(!Validate.Required(EmailAddress, Label: "Email Address")){return;}
        
        if(!Validate.MaxLength(UserFirstName, length: 10, Label: "First Name")){return;}
        if(!Validate.MaxLength(UserLastName, length: 10, Label: "Last Name")){return;}
        
                
        // Post Model Create
        ShoutoutAPI.UpdateUserInfo(UserFirstName, last_name: UserLastName, email_address: EmailAddress, image_string: pickedBase64Image) { (callback) in
            //
            if(callback){
                    self.navigationController?.popViewControllerAnimated(false)
//                UtilityHelper.AlertMessagewithCallBack("Updated Successfully", success: {
//                    
//                })
            }
        }

        
        
        
        
        
        
        
    }
    

    
    override func viewDidAppear(animated: Bool) {
        // hide tab bar for this view
        self.tabBarController?.tabBar.hidden = true
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
