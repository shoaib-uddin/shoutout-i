//
//  LoginViewController.swift
//  ShoutOut
//
//  Created by Fez on 22/03/2016.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit
import Foundation

class LoginViewController: BaseViewController {

    //Page Controls
    @IBOutlet weak var tbxUserName: UITextField!
    @IBOutlet weak var tbxPassword: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var newAcoountButton: UIButton!
    
    let deviceTokenString = UserModel.getKey("device_token");
    
    
    
    
    
    
    
    
    
    
    override func viewDidAppear(animated: Bool) {
         hideTopBar();
    }
    override func viewWillAppear(animated: Bool) {
        hideTopBar();
    }
   
    func hideTopBar(){
        self.navigationController?.navigationBar.hidden = true;
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //only for login
        navigationController?.navigationBar.barStyle = UIBarStyle.Black
        //ShowSystemAlert("test");
        
        // CSSLayout called
        setUITextViewCons(tbxUserName, icon: "user-seperator@1x", showIcon: true);
        setUITextViewCons(tbxPassword, icon: "lock@1x", showIcon: true);
        setUIButtonCons(loginButton);
        //setBottomBorder(newAcoountButton);
    
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    //MARK: - TextFieldDelegates
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        self.view.endEditing(true)
        return true
    }
    
    func getKey(KeyName: String, arrUserPreferncesKeys:[UserPrefencesSettings])-> String{
        var keyValue = settings.DistanceText.meter;
        let filteredData =   arrUserPreferncesKeys.filter { (e: UserPrefencesSettings) -> Bool in
            e.key_name == KeyName
        }
        if(filteredData.count > 0){
            keyValue = filteredData[0].key_value!;
        }
        return keyValue;
    }
    
    func LoadSettings(
        success: ((arrUserPreferncesKeys :[UserPrefencesSettings]) -> Void)!
        ){
        
        //Network Request
        NetworkHelper.MakeGetRequestForArray("/ShoutOut_get_user_preferences/", postData: nil, showLoader: false, success: { (successData) -> Void in
            let  arrUserPreferncesKeys = [UserPrefencesSettings](json: successData)
            success?(arrUserPreferncesKeys: arrUserPreferncesKeys);
            
            
            },failure: { (error) -> Void in
        })
        
    }
    
    @IBAction func btnForgetPassswordTapped(sender: UIButton) {
        UtilityHelper.NavigateToViewController(self, viewControllerid: "ForgotPasswordViewController")
    }
    
    @IBAction func btnSignupTapped(sender: UIButton) {
        UtilityHelper.NavigateToViewController(self, viewControllerid: "SignupLaunchViewController")
    }
    
    
    //Login Events
    @IBAction func LoginPressed(sender: UIButton) {
        
        // Initializing Items
        let UserName: String = tbxUserName.text!  as String;
        let Password: String = tbxPassword.text! as String;
        
        //doing Validations
        if(!Validate.Required(UserName, Label: "User Name")){return;}
        if(!Validate.Required(Password, Label: "Password")){return;}

        ShoutoutAPI.ShoutoutLogin(UserName, Password: Password, completion: {(callback) in
            if(callback){
                UtilityHelper.NavigateToDashboard(self);
            }else{
                UtilityHelper.AlertMessage("Invalid username or password!");
            }
        })
        
    }
    

   
}
