//
//  AuthenticationController.swift
//  ShoutOut
//
//  Created by Fez on 3/21/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit
import EVReflection

class AuthenticationController: BaseViewController {

    //Page Outlets
    
    @IBOutlet weak var tbxPhoneNumber: UITextField!
    @IBOutlet weak var tbxCarrier: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Step 1"
        
        #if DEBUG
    
        tbxPhoneNumber.text = "6315147444";
        tbxCarrier.text = "fa2d4d4e579679b";
        
        #endif
        
        self.navigationController?.navigationBar.hidden = false;
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func AuthenticateButtonClicked(sender: UIButton) {
       
       // Initializing Items
        let phone_number: String = tbxPhoneNumber.text!  as String;
        let authorization_code: String = tbxCarrier.text! as String;
        
        //doing Validations
        if(!Validate.Required(phone_number, Label: "Phone Number")){return;}
        if(!Validate.Required(authorization_code, Label: "Authorization Code")){return;}
        
        
        // Post Model Create
        let postModel =
        [ "phone_number":  "\(phone_number)",
            "authorization_code":  "\(authorization_code)"];
        
        //Network Request
        NetworkHelper.MakePostRequest("/ShoutOut_verify_phone_number", postData: postModel, showLoader: true, success: { (successData) -> Void in
            let response =  ValidPhoneResponse(dictionary: successData as! NSDictionary)
            let navigationController : VerifyCodeViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("VerifyCodeViewController") as? VerifyCodeViewController)!
            navigationController.accountId = response.account_id!;
            navigationController.phoneNumber = phone_number;
            navigationController.authorizationCode = authorization_code
            self.navigationController!.pushViewController(navigationController, animated: true)
           },failure: { (error) -> Void in
            })
      
    }
    

    
    @IBAction func btnHelpTapped(sender: UIButton) {
        
        ShowInformationWithTitle("", AlertText: "")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
