//
//  SignUpViewController.swift
//  ShoutOut
//
//  Created by Fez on 22/03/2016.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {
    
    //Page Controls
    @IBOutlet weak var tbxUserName: UITextField!
    
    @IBOutlet weak var tbxPassword: UITextField!
    
    @IBOutlet weak var tbxRetypePassword: UITextField!
    
    @IBOutlet weak var tbxFullName: UITextField!
    
    @IBOutlet weak var tbxEmailAddress: UITextField!
    
    
    var sessionID: String = "";
    var accountID: String = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Step 3"
        debugPrint(sessionID);
        debugPrint(UtilityHelper.generateDeviceToken());
        
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.hidesBackButton = true;
        
    }

    @IBAction func RegistrationButtonClicked(sender: UIButton) {
        
        // Initializing Items
        let UserName: String = tbxUserName.text!  as String;
        let Password: String = tbxPassword.text! as String;
        let RetypePassword: String = tbxRetypePassword.text!  as String;
        let FullName: String = tbxFullName.text! as String;
        let EmailAddress: String = tbxEmailAddress.text!  as String;
        
        //doing Validations
        if(!Validate.Required(UserName, Label: "User Name")){return;}
        if(!Validate.Required(Password, Label: "Password")){return;}
        if(!Validate.Required(RetypePassword, Label: "Confirm Password")){return;}
        if(!Validate.Required(FullName, Label: "Full Name")){return;}
        if(!Validate.Required(EmailAddress, Label: "Email Address")){return;}
        if(!Validate.Email(EmailAddress, Label: "Email Address")){return;}
        
        if(!Validate.Length(Password, length:6,Label: "Password")){return;}
        
        if(!Validate.MatchStrings(Password, Text2: RetypePassword, Label1: "Password", Label2: "Confirm Password")){return;}
        
        
        // Post Model Create
        let postModel =
        [ "username":  "\(UserName)",
            "name":  "\(FullName)",
            "email":  "\(EmailAddress)",
            "password":  "\(Password)"
        ];
        
        //Headers
         let header =   [ "account_id":  "\(accountID)", "session_id":  "\(sessionID)"];
        
        //Network Request
        NetworkHelper.MakePostRequest("/ShoutOut_register_user", postData: postModel, showLoader: true, success: { (successData) -> Void in
//            let navigationController : TermsOfUseViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("TermsOfUseViewController") as? TermsOfUseViewController)!
//            
//            navigationController.accountID = self.accountID;
//            navigationController.sessionID = self.sessionID;
//            navigationController.email = EmailAddress;
//            navigationController.userName = UserName;
//            navigationController.password = Password;
//            navigationController.fullname = FullName;
//            navigationController.phonenumber = "";
            
            
             //  self.navigationController!.pushViewController(navigationController, animated: true)
            },failure: { (error) -> Void in
        },headers : header)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}