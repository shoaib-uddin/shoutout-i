//
//  SignUpPartTwoViewController.swift
//  shoutOut
//
//  Created by Fez on 6/7/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

class SignUpPartTwoViewController : BaseViewController, UIImagePickerControllerDelegate,
    UINavigationControllerDelegate
    {
   
    
    
    @IBOutlet weak var OuterImageView: UIView!
    @IBOutlet weak var tbxProfilePictureUpload: UIImageView!
    @IBOutlet weak var tbxUserTag: UITextField!
    @IBOutlet weak var tbxRegDone: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var lblUploadPhoto: UILabel!
   
    
    
    let recognizer = UITapGestureRecognizer();
    let imagePicker = UIImagePickerController();
    
    
    
    var SignupFirstName: String = "";
    var SignupLastName: String = "";
    var SignupEmail: String = "";
    var SignupPassword: String = "";
    var pickedBase64Image: String = "";
    var deviceTokenString: String = "";
    var deviceLat: Double = 0;
    var deviceLong: Double = 0;
    var deviceAltitude: Double = 0;

    override func viewDidAppear(animated: Bool) {
        
    }
    
    @IBAction func backButtonFromSignup(sender: UIBarButtonItem) {
        //UtilityHelper.NavigateToViewController(self, viewControllerid: "SignUpPartOneViewController");
        self.dismissViewControllerAnimated(false, completion: nil);
        //self.view.popViewControllerAnimated(true)
        
    }
    
    override func viewDidLoad() {
        
        // calling functions for CSS changes
        setUITextViewCons(tbxUserTag, icon: "a", showIcon: false);
        setUIButtonCons(tbxRegDone);
        transInnerWhiteOuter(OuterImageView);
        whiteBackgroundCircularImage(tbxProfilePictureUpload);
        addTextLayerOnImageCenter(lblUploadPhoto);
        
        // getting values from signup first page
        
        
        
        
        
        // image TAP
        profileImage.userInteractionEnabled = true
        recognizer.addTarget(self, action: #selector(SignUpPartTwoViewController.plusBtnTapped))
        profileImage.addGestureRecognizer(recognizer)
        
        // IMAGE DELEGATE
        imagePicker.delegate = self;
        
        
    }

    
    
    func plusBtnTapped(sender: UITapGestureRecognizer) {
        
        let alert = UIAlertController(title: "Select Picture Option", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let libButton = UIAlertAction(title: "Select photo from library", style: UIAlertActionStyle.Default) {  (alert) -> Void in
            
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.imagePicker.allowsEditing = true
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            let cameraButton = UIAlertAction(title: "Take a picture", style: UIAlertActionStyle.Default) {  (alert) -> Void in
                print("Take Photo")
                
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
                self.imagePicker.allowsEditing = true
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
                
            }
            alert.addAction(cameraButton)
        } else {
            print("Camera not available")
            
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {  (alert) -> Void in
            print("Cancel Pressed")
        }
        
        alert.addAction(libButton)
        alert.addAction(cancelButton)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    
    //Image Picker Delegates
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        if let pickedImage = editingInfo![UIImagePickerControllerEditedImage] as? UIImage {
            
            profileImage.image = pickedImage
            pickedBase64Image = UtilityHelper.ImageToBase64String(pickedImage);
            
        }
        dismissViewControllerAnimated(true, completion: nil);
        
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            //hide label after picture choosen
            lblUploadPhoto.hidden = true;
            
            profileImage.image = pickedImage
            
            
            
            pickedBase64Image = UtilityHelper.ImageToBase64String(pickedImage);
            print(pickedBase64Image);
            
            
        }
        dismissViewControllerAnimated(true, completion: nil);
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func btnCallSignup(sender: UIButton) {
        
        
        // Initializing Items
        let UserTag: String = tbxUserTag.text!  as String;
        let UserImage: String = pickedBase64Image;
        //doing Validations
        if(!Validate.Required(UserTag, Label: "your tag")){return;}
        if(!Validate.NoWhiteSpace(UserTag, Label: "")){return;}
        if(!Validate.alphanumeric(UserTag, Label: "")){return;}
        if(!Validate.Required(UserImage, Label: "your image")){return;}
        
        ShoutoutAPI.RegisterUser(SignupFirstName, SignupLastName: SignupLastName, SignupEmail: SignupEmail, SignupPassword: SignupPassword, UserImage: UserImage, UserTag: UserTag, deviceTokenString: deviceTokenString, deviceLat: deviceLat, deviceLong: deviceLong, deviceAltitude: deviceAltitude) { (callback) in
            //
            if(callback){
                 UtilityHelper.NavigateToViewController(self, viewControllerid: "DashboardTabBarController");
            }else{
                 UtilityHelper.AlertMessage("something wrong, please try again");
            }
        }
        
                
    }
    
    
    
    
    
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    
    
    

}