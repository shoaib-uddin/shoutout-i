//
//  VerifyCodeViewController.swift
//  ShoutOut
//
//  Created by Fez on 22/03/2016.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

class VerifyCodeViewController: BaseViewController , UITextFieldDelegate{
    
    @IBOutlet weak var pass1 : UITextField!
    @IBOutlet weak var pass2 : UITextField!
    @IBOutlet weak var pass3 : UITextField!
    @IBOutlet weak var pass4 : UITextField!
    
    
    var accountId : String = "";
    var phoneNumber : String = "";
    var authorizationCode : String = "";
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Step 2"
        
        // Do any additional setup after loading the view.
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func VerifyButtonClicked(sender: UIButton) {
        
        //value capture
        let pass1val: String = pass1.text!  as String;
        let pass2val: String = pass2.text!  as String;
        let pass3val: String = pass3.text!  as String;
        let pass4val: String = pass4.text!  as String;
        
        
        //validation
        if(!Validate.Required(pass1val, Label: "All Boxes")){return;}
        if(!Validate.Required(pass2val, Label: "All Boxes")){return;}
        if(!Validate.Required(pass3val, Label: "All Boxes")){return;}
        if(!Validate.Required(pass4val, Label: "All Boxes")){return;}
        
        if(!Validate.isInteger(pass1val, Label: "All Boxes")){return;}
          if(!Validate.isInteger(pass2val, Label: "All Boxes")){return;}
          if(!Validate.isInteger(pass3val, Label: "All Boxes")){return;}
          if(!Validate.isInteger(pass4val, Label: "All Boxes")){return;}
        
        
        //custom logic
        let pin_code = pass1val + pass2val + pass3val + pass4val
        
        let postModel =
        [ "pin_code":  "\(pin_code)"];
        
        let header =   [ "account_id":  "\(accountId)"];
        
        
        //Network Request
        NetworkHelper.MakePostRequest("/ShoutOut_verify_pin", postData: postModel, showLoader: true, success: { (successData) -> Void in
            let response =  PinResponse(dictionary: successData as! NSDictionary)
            let navigationController : SignUpViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("SignUpViewController") as? SignUpViewController)!
            
            if(response.verified == 1){
                navigationController.sessionID = response.session_id!;
                navigationController.accountID = self.accountId;
            
            }
            else{
                UtilityHelper.AlertMessage("Please Enter Correct Pin");
            }
            
              self.navigationController!.pushViewController(navigationController, animated: true)
            },failure: { (error) -> Void in
            },headers: header)
        

        
        
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let isTrue : Bool = !(textField.text!.characters.count >= 1 && string.characters.count > range.length)
        
        if textField.text!.characters.count == 0{
            
            self.performSelector("switchTextFieldWithTag:", withObject: textField, afterDelay: 0.1)
        }
        
       return isTrue
    
    }
    
    @IBAction func btnResendClicked(sender: UIButton) {
        
        // Post Model Create
        let postModel =
        [ "phone_number":  "\(phoneNumber)",
            "authorization_code":  "\(authorizationCode)"];
        
        //Network Request
        NetworkHelper.MakePostRequest("/ShoutOut_verify_phone_number", postData: postModel, showLoader: true, success: { (successData) -> Void in
            let response =  ValidPhoneResponse(dictionary: successData as! NSDictionary)
            let navigationController : VerifyCodeViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("VerifyCodeViewController") as? VerifyCodeViewController)!
            navigationController.accountId = response.account_id!;
          
            self.ShowInformation("We have re-sent the 4 digit code.You should receive it shortly.")
            
            
            },failure: { (error) -> Void in
        })
        
    }
    
        
        
    
    func switchTextFieldWithTag (textField:UITextField){
        
        switch textField.tag{
        case 1:
            pass1.resignFirstResponder()
            pass2.becomeFirstResponder()
            break
        case 2:
            pass2.resignFirstResponder()
            pass3.becomeFirstResponder()
            break
        case 3:
            pass3.resignFirstResponder()
            pass4.becomeFirstResponder()
            break
        case 4:
            pass4.resignFirstResponder()
            break
        default:
            break
            
        }
    }
}
