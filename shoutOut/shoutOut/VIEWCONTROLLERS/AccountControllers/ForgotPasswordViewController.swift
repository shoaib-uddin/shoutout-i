//
//  ForgotPasswordViewController.swift
//  ShoutOut
//
//  Created by Fez on 22/03/2016.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {

    //Page Controls
    
    @IBOutlet weak var tbxEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = false;
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ResetPasswordClicked(sender: UIButton) {
        // Initializing Items
        let Email: String = tbxEmail.text!  as String;
        
        //doing Validations
        if(!Validate.Required(Email, Label: "Email")){return;}
        if(!Validate.Email(Email, Label: "Email Address")){return;}
        
        ShoutoutAPI.ForgotPassword(Email, completion: {(isEmailSent) in
            if(isEmailSent){
                UtilityHelper.AlertMessagewithCallBack("An email has been Sent to your Email Account. please follow that email", success: { () -> Void in
                    self.navigationController?.popViewControllerAnimated(true);
                    
                })

            }
        })
        
                
    }

    //MARK: - TextFieldDelegates
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        
        self.view.endEditing(true)
        
        return true
    }
    

}
