//
//  SignUpPartOneViewController.swift
//  shoutOut
//
//  Created by Fez on 6/7/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit

class SignUpPartOneViewController : BaseViewController {

    
    @IBOutlet weak var tbxFirstName: UITextField!
    @IBOutlet weak var tbxLastName: UITextField!
    @IBOutlet weak var tbxPassword: UITextField!
    @IBOutlet weak var tbxEmail: UITextField!
    @IBOutlet weak var btnSignupNext: UIButton!
    
    @IBAction func goBackToLogin(sender: UIButton) {
        self.dismissViewControllerAnimated(false, completion: nil);
    }
    
    
    
    
    
    let deviceTokenString = UserModel.getKey("device_token");
        
    override func viewDidLoad() {
        
        // calling functions for CSS changes
        setUITextViewCons(tbxFirstName, icon: "a", showIcon: false);
        setUITextViewCons(tbxLastName, icon: "a", showIcon: false);
        setUITextViewCons(tbxEmail, icon: "a", showIcon: false);
        setUITextViewCons(tbxPassword, icon: "a", showIcon: false);
        setUIButtonCons(btnSignupNext);
        
        //print(deviceTokenString);
        
        
    }
    
    
    // signup next button pressed
    @IBAction func btnSignupNext(sender: UIButton) {
        
        let EmailAddress: String = tbxEmail.text!  as String;
        if(!Validate.Required(EmailAddress, Label: "Email Address")){return;}
        
        ShoutoutAPI.checkIfEmailExists(EmailAddress, completion: { (isEmailExist)  in
            
            if(isEmailExist){
                UtilityHelper.AlertMessage("Email already Exists");
            }else{
                self.gotoSignupPartTwo();
            }
            
        })
        
        
    }
    
    
    
    
    
    func gotoSignupPartTwo() {
        
        let loc = currLocation;
        
        // Initializing Items
        let UserFirstName: String = tbxFirstName.text!  as String;
        let UserLastName: String = tbxLastName.text!  as String;
        let EmailAddress: String = tbxEmail.text!  as String;
        let Password: String = tbxPassword.text! as String;
        
        //doing Validations
        if(!Validate.Required(UserFirstName, Label: "First Name")){return;}
        if(!Validate.Required(UserLastName, Label: "Last Name")){return;}
        if(!Validate.Required(EmailAddress, Label: "Email Address")){return;}
        if(!Validate.Email(EmailAddress, Label: "Email Address")){return;}
        if(!Validate.Required(Password, Label: "Password")){return;}
        if(!Validate.NoWhiteSpace(Password, Label: "")){return;}
        if(!Validate.alphanumeric(Password, Label: "")){return;}
        
        // length validation
        if(!Validate.MaxLength(UserFirstName, length: 10, Label: "First Name")){return;}
        if(!Validate.MaxLength(UserLastName, length: 10, Label: "Last Name")){return;}
        if(!Validate.MaxLength(Password, length: 15, Label: "Password")){return;}
        
        self.view.endEditing(true) // closes keyboard first
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let destination = storyboard.instantiateViewControllerWithIdentifier("SignUpPartTwoViewController") as! SignUpPartTwoViewController
        
        destination.SignupFirstName = UserFirstName;
        destination.SignupLastName = UserLastName;
        destination.SignupEmail = EmailAddress;
        destination.SignupPassword = Password;
        destination.deviceTokenString = deviceTokenString;
        destination.deviceLat = loc.coordinate.latitude;
        destination.deviceLong = loc.coordinate.longitude;
        destination.deviceAltitude = loc.altitude
        
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    
    
    @IBAction func backButtonFromSignup(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
    
    }
    

}


