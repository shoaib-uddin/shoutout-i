//
//  ForgetPasswordController.swift
//  shoutOut
//
//  Created by clines193 on 7/13/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

class ForgetPasswordController : BaseViewController, UIImagePickerControllerDelegate,
    UINavigationControllerDelegate
{
    
    @IBOutlet weak var tbxUserEmail: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnBack: UIButton!
   
    
    
    var forgetUserEmail: String = "";
    
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    @IBAction func backButtonFromSignup(sender: UIBarButtonItem) {
        UtilityHelper.NavigateToViewController(self, viewControllerid: "LoginViewController");
    }
    
    @IBAction func GoBackToLogin(sender: UIButton) {
        self.dismissViewControllerAnimated(false, completion: nil)
        print("cancel");
    }
    override func viewDidLoad() {
        
        // calling functions for CSS changes
        setUITextViewCons(tbxUserEmail, icon: "a", showIcon: false);
        setUIButtonCons(btnSend);
        
    }
    
    
    @IBAction func btnCallForgetPassword(sender: UIButton) {
        
        // Initializing Items
        let UserEmail: String = tbxUserEmail.text!  as String;
        
        
        //doing Validations
        if(!Validate.Required(UserEmail, Label: "your email")){return;}
        if(!Validate.Email(UserEmail, Label: "email")){return;}
        
        
        ShoutoutAPI.sendForgetPasswordEmail(UserEmail, completion: { (isEmailSent)  in
            
            if(isEmailSent){
                UtilityHelper.AlertMessagewithCallBack("Please Check your email", success: {(response) -> Void in
                    self.dismissViewControllerAnimated(false, completion: nil)
                    
                })
            }
            
        })
                
                
    }
    
    
    
    
    
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

