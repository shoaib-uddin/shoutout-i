//
//  SignupLaunchViewController.swift
//  ShoutOut
//
//  Created by Fez on 22/03/2016.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

class SignupLaunchViewController: BaseViewController {
    
    
    
    var timer = NSTimer()
    var NSTimeInterval = 2.0;
    var myImages = ["round@1x", "loop@1x", "edit-round@1x", "round@1x"];
    var myLabels = ["Tag Someone & Create a new Shoutout!",
                    "view infinite Shoutouts from your area",
                    "Sign up & create a mini Profile",
                    "Tag Someone & Create a new Shoutout!"];
    
    @IBOutlet weak var animRoundImage: UIImageView!
    @IBOutlet weak var animTextLabel: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.hidesBackButton = true;
        
        
        //call a timer
        timer = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval, target: self, selector: #selector(SignupLaunchViewController.changeImage), userInfo: "", repeats: true);
        timer.fire()
        
    }
    
    
    
    var i: Int = 0
    
    
    
    // change image function
    func changeImage() {
        //background.alpha=0.5;
        UIImageView.beginAnimations(nil, context: nil)
        UIImageView.setAnimationDuration(0.6)
        animRoundImage.alpha = 1
        animRoundImage.image = UIImage(named: myImages[i]+"")
        UIImageView.commitAnimations();
        let string = myLabels[i];
        animTextLabel.attributedText = NSAttributedString(string: string);
        
        
        //animTextLabel.attributedText = myLabels[i];
        i += 1
        if (i == myImages.count ){
            
            timer.invalidate();
            UtilityHelper.NavigateToViewController(self, viewControllerid: "SignUpPartOneViewController")
            
            
        }
        
        
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}