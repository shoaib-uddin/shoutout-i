//
//  ShareViewController.swift
//  shoutOut
//
//  Created by Fez on 6/7/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit
import Social
import MessageUI
import ContactsUI


class ShareViewController: BaseViewController ,MFMessageComposeViewControllerDelegate, CNContactPickerDelegate  {

    @IBOutlet weak var tbxShare: UITextField!
    @IBOutlet weak var btnFacebookShare: UIButton!
    @IBOutlet weak var btnTwitterShare: UIButton!
    @IBOutlet weak var btnSkypeShare: UIButton!
    
    let info = UserModel.GetInfo();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //CSS Layouts functions
        setUITextViewCons(tbxShare, icon: "@seperator@1x", showIcon: true);
        
    }
    
    
    @IBAction func btnShareClicked(sender: UIButton) {
        
        if(!Validate.Email(tbxShare.text!, Label: "Email")){return;}
        
        let UserEMAIL: String = tbxShare.text!  as String;
        
        ShoutoutAPI.SendInvitationEmail(UserEMAIL) { (callback) in
            //
            UtilityHelper.AlertMessagewithCallBack("Email send successfully", success: {
                self.navigationController?.popViewControllerAnimated(false)
                
            })
        }
        
        
        
        
    }
    
  
    
    
    @IBAction func FacebookShare(sender: UIButton) {
        
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            
            let fbController = SLComposeViewController(forServiceType: SLServiceTypeFacebook);
            fbController.setInitialText("Shoutout post test");
            fbController.addURL(NSURL(string: "http://developers.facebook.com"));
            self.presentViewController(fbController, animated: true, completion: nil);
            
        }else{
            alertToSettings("facebook");
        }
        
    }
    
    
    @IBAction func TwitterShare(sender: UIButton) {
        
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
            
            let tweetController = SLComposeViewController(forServiceType: SLServiceTypeTwitter);
            tweetController.setInitialText("Shoutout tweet test");
            tweetController.addURL(NSURL(string: "http://developers.facebook.com"));
            self.presentViewController(tweetController, animated: true, completion: nil);
            
        }else{
           alertToSettings("twitter")
        }
        
    }
    
    
    
    
    
    @IBAction func SkypeShare(sender: UIButton){
        showContacts();
    }
    
    
    func showContacts(){
        
        let contactPickerViewController = CNContactPickerViewController()
        contactPickerViewController.delegate = self
        presentViewController(contactPickerViewController, animated: true, completion: nil)
        
    }
    
    
    //MARK:CONTACT Picker Delegate Methods
    func contactPicker(picker: CNContactPickerViewController, didSelectContact contact: CNContact)
    {
        
        ContactList.removeAll();
        
        for phoneNumber:CNLabeledValue in contact.phoneNumbers {
            let a = phoneNumber.value as! CNPhoneNumber
            ContactList.append(a.stringValue);
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            self.openMsgWindow(self.ContactList)
        };
        
    }
    
    func contactPickerDidCancel(picker: CNContactPickerViewController) {
        
    }
    
    func openMsgWindow(ContactList: Array<String>){
        
        let messageComposer = MessageComposer();
        if (messageComposer.canSendText()) {
            // Obtain a configured MFMessageComposeViewController
            let messageComposeVC = configuredMessageComposeViewController(ContactList)
            presentViewController(messageComposeVC, animated: true, completion: nil)
            
        } else {
            // Let the user know if his/her device isn't able to send text messages
            let errorAlert = UIAlertView(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        }
        
    }
    
    
    // A wrapper function to indicate whether or not a text message can be sent from the user's device
    func canSendText() -> Bool {
        return MFMessageComposeViewController.canSendText()
    }
    
    var ContactList : Array<String> = Array<String>();
    
    // Configures and returns a MFMessageComposeViewController instance
    func configuredMessageComposeViewController(ContactList: Array<String>) -> MFMessageComposeViewController {
        
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self  //  Make sure to set this property to self, so that the controller can be dismissed!
        messageComposeVC.recipients = [ContactList.first!];
        messageComposeVC.body = "Hey friend - I am sending this message from shoutout App, come join me!"
        messageComposeVC.messageComposeDelegate = self
        
        return messageComposeVC
    }
    
    
    
    
    // MFMessageComposeViewControllerDelegate callback - dismisses the view controller when the user is finished with it
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    func alertToSettings(type: String){
        
        let alert = UIAlertController(title: "Accounts", message: "Please login to \(type) in your phone" , preferredStyle: UIAlertControllerStyle.Alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { action in
            
        }))
        alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.Cancel, handler: { action in
            
            // do something like...
            let settingURL = NSURL(string: UIApplicationOpenSettingsURLString)
            
            if let url = settingURL{
                UIApplication.sharedApplication().openURL(url);
            }
            
        }))
        
        // show the alert
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = false;
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
