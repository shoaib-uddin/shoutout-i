//
//  FollowViewController.swift
//  shoutOut
//
//  Created by Fez on 6/7/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit

class FollowViewController: BaseViewController,FollowCellDelegate, UINavigationControllerDelegate,UITableViewDataSource , UITableViewDelegate{

    var followList : Array<FollowModel> = Array<FollowModel>();
    @IBOutlet weak var tableView: UITableView!
    

    func unFollowUser(cell: GetAllFollowsTableViewCell) {
        // create the alert
        
        
        let c = followList[cell.indexPathRow].first_name! + " " + followList[cell.indexPathRow].last_name!
        
        let alert = UIAlertController(title: "unfollow", message: "Would you like to unfollow " + "\(c)" , preferredStyle: UIAlertControllerStyle.Alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: { action in
            
            // do something like...
            
            ShoutoutAPI.followFeedOfThisUser(self.followList[cell.indexPathRow].following_id!, isFollow: "false", completion: {(callback) in
                self.tableView.beginUpdates();
                self.followList.removeAtIndex(cell.indexPathRow);
                
                self.tableView.deleteRowsAtIndexPaths(NSArray(object: NSIndexPath(forRow: cell.indexPathRow, inSection: 0)) as! [NSIndexPath], withRowAnimation: UITableViewRowAnimation.Left)
                self.tableView.endUpdates()
            
            })
            
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { action in
            
            // do something like...
            print("cancel")
            
        }))
        
        // show the alert
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Followers";
        
        tableView.registerNib(UINib(nibName: "GetAllFollowsTableViewCell", bundle: nil), forCellReuseIdentifier: "GetAllFollowsTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 400;
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        // call a function to get feeds
        
        get_all_user_follows();
        
    }
    
    func get_all_user_follows(){
        
        
        
        //Network Request
        NetworkHelper.MakeGetRequestForArray("shoutout_get_all_followings", showLoader: true, success: { (successData) -> Void in
            
            self.followList = [FollowModel](json: successData);
            self.tableView.reloadData();
            
            },failure: { (error) -> Void in
                _ = error
                
        })
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = false;
        
    }
    
    //Delegation Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return followList.count;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("GetAllFollowsTableViewCell", forIndexPath: indexPath) as! GetAllFollowsTableViewCell
        cell.indexPathRow = indexPath.row;
        cell.setData(followList[indexPath.row]);
        cell.cellDDelegate = self;
        return cell;
        
    }

    
    
    
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
