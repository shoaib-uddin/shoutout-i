//
//  SplashViewController.swift
//  ShoutOut
//
//  Created by Fez on 22/03/2016.
//  Copyright © 2016 Matech. All rights reserved.
//

import UIKit
import ReachabilitySwift;
import INTULocationManager;

class SplashViewController: UIViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: "showNextViewController", userInfo: nil, repeats: false)
       

    }

    override func viewDidAppear(animated: Bool) {
        
        self.showNextViewController()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showNextViewController(){

        if(!UserModel.isUserLoggedIn()){
            UtilityHelper.NavigatetoNavigationController(self, ViewStoryBoardID: "LoginNavigationController");
        }
        else
        {
            UtilityHelper.NavigateToDashboard(self);
        }
    
    }

}
