//
//  MainTabBarController.swift
//  ShoutOut
//
//  Created by Fez on 4/27/16.
//  Copyright © 2016 Matech. All rights reserved.
//
import Foundation
import UIKit

class MainTabBarController: UITabBarController , UITabBarControllerDelegate {

    var baseHeight: CGFloat = 60
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        
        return true;
   
    
    
    }
    
    override func viewWillLayoutSubviews() {
        var tabFrame: CGRect = self.tabBar.frame
        tabFrame.size.height = baseHeight
        tabFrame.origin.y = self.view.frame.size.height - baseHeight
        self.tabBar.frame = tabFrame
        
        storeTabIndex();
        
    }
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        storeTabIndex();
    }
    
    
    func storeTabIndex(){
        
        var selectedIndex:Int = self.selectedIndex
        var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if(selectedIndex<0){
            appDelegate.previousSelectedTabIndex = 0;
        }else{
            appDelegate.previousSelectedTabIndex = selectedIndex;
        }
        
        
    }
    
    override func viewDidLoad() {
        self.delegate = self;
        
        // Sets the background color of the selected UITabBarItem (using and plain colored UIImage with the width = 1/5 of the tabBar (if you have 3 items) and the height of the tabBar)
        
        var selectedImage = UIImage().makeImageWithColorAndSize(UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1), size: CGSizeMake(self.tabBar.frame.width/5, baseHeight));
        
        UITabBar.appearance().selectionIndicatorImage = selectedImage
        UITabBar.appearance().tintColor = UIColor(red: 41/255.0, green: 130/255.0, blue: 159/255.0, alpha: 0.8);
        UITabBar.appearance().barTintColor = UIColor(red: 41/255.0, green: 130/255.0, blue: 159/255.0, alpha: 0.8);
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 41/255.0, green: 130/255.0, blue: 159/255.0, alpha: 0.8)], forState: .Selected)
        
        // Uses the original colors for your images, so they aren't not rendered as grey automatically.
        for item in self.tabBar.items! as [UITabBarItem] {
            if let image = item.image {
                //item.image = image.imageWithRenderingMode(.AlwaysOriginal)
                item.image = image.imageWithColor(UIColor.whiteColor()).imageWithRenderingMode(.AlwaysOriginal)
            }
        }
        
        
   
    
    }
    
    
    
    
        
    
    
    
    
    
    
}
