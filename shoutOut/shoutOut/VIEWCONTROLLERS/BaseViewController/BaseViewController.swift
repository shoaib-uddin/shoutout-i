//
//  BaseViewController.swift
//  ShoutOut
//
//  Created by Fez on 3/21/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit
import INTULocationManager

class BaseViewController: UIViewController , PopupActionDelegate {
    
    override func viewDidLoad() {
        changeNavigationTheme();
        createBackButton();
        LogMessage("Navigating to " + self.nameOfClass  + " Screen", Log_Type: LogType.MAIN)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        
    }
    
//    func getMyLocation() -> CLLocation!{
//        
//        var c = AppDelegate.getAppDelegate().currLocation;
//        return c;
//        
//        
//        
//    }
    
    func setNormalTitle(titleName : String){
        self.title = titleName;
    }
    
    func setTitlewithAppearance(titleName : String){
        self.title = titleName;
    }

  
    
    func changeNavigationTheme(){
  
        navigationController?.navigationBar.barTintColor = colorWithHexString("#ffffff");
       //navigationController?.navigationBar.tintColor =  colorWithHexString("#124dc6")
       
        
        // Font Change Required Commented it for later
        //  navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Avenir Book", size: 15)!]
        navigationController?.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName : colorWithHexString("#000000")];
        
        navigationController?.navigationBar.opaque = false
       navigationController?.navigationBar.barStyle = UIBarStyle.Default
        navigationController?.navigationBar.translucent = false
    }
    

    func popOutController(){
        self.navigationController?.popViewControllerAnimated(true);
    }

    
    func createBackButton(){
    
        if (self.navigationController?.viewControllers.count>1){
            
            let menuBackItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "left-arrow@1x"), style: UIBarButtonItemStyle.Bordered, target: self, action: #selector(BaseViewController.popOutController))
            self.navigationItem.leftBarButtonItem = menuBackItem
    
           //NO Bottom Border required in Back Screen
            navigationController?.navigationBar.barStyle = UIBarStyle.Black
        }
        else
        {
        navigationController?.navigationBar.barStyle = UIBarStyle.Default
        }
    
    }
    
    
    
    @objc func btnCloseTapped(sender: UIButton) {
        close2PopUp(sender);
        
       }
    
    @objc func btnActionTapped(sender: UIButton) {
        
        close2PopUp(sender);
    
    }
    
    func close2PopUp(sender: UIButton)
    {
        let vw = sender.superview!.superview!.viewWithTag(1)! as UIView
        vw.removeFromSuperview()
    }
    


    
    func ShowConfirmPopUp(btnName : String, AlertText : String)
    {
        
        let screenSize : CGRect = UIScreen.mainScreen().bounds;
        let FrameSize : CGRect = CGRectMake(0,0,screenSize.width,screenSize.height)
        let backgroundView : UIView = UIView(frame: FrameSize)
        backgroundView.tag = 1;
        backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        let window: UIWindow! = UIApplication.sharedApplication().delegate!.window!
        
        
        let vw: PopupView =  NSBundle.mainBundle().loadNibNamed("PopupView", owner: self, options: nil)[0] as! PopupView
        
        vw.frame=CGRectMake(20,((screenSize.size.height/2) - (vw.frame.size.height)/2), screenSize.size.width - 40, vw.frame.size.height )
        
        vw.btnOne.setTitle("Cancel", forState: UIControlState.Normal);
        
        vw.btnTwo.setTitle(btnName, forState: UIControlState.Normal);
        vw.lblpopUpText.text = AlertText
        
        vw.popUpDelegate = self;
        backgroundView.addSubview(vw);
        window.addSubview(backgroundView)
        
    }
    
    func ShowInformationWithTitle(var Header : String, AlertText : String, Attributed:Bool = false)
    {
        
        let screenSize : CGRect = UIScreen.mainScreen().bounds;
        let FrameSize : CGRect = CGRectMake(0,0,screenSize.width,screenSize.height)
        let backgroundView : UIView = UIView(frame: FrameSize)
        backgroundView.tag = 1;
        backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        let window: UIWindow! = UIApplication.sharedApplication().delegate!.window!
        
        
        let vw: InformationView =  NSBundle.mainBundle().loadNibNamed("InformationView", owner: self, options: nil)[0] as! InformationView
        
        vw.frame=CGRectMake(20,((screenSize.size.height/2) - (vw.frame.size.height)/2), screenSize.size.width - 40, vw.frame.size.height )
        
        vw.btnClose.setTitle("Close", forState: UIControlState.Normal);
        
        
        if (Header != ""){
        
            if(!Attributed){
                vw.lblHeading.text = Header
            }
            else{
                Header = "<span style='font-weight:bold;'>" + Header  + "</span>"
                vw.lblHeading.attributedText =  Header.ToNSAttributedString(24, showBold: true);
          
                
             }
        
        }
        
         if (AlertText != ""){
            if(!Attributed){

                vw.lblMessage.text = AlertText
            }
            else{
                vw.lblMessage.attributedText =  AlertText.ToNSAttributedString(14);
          
            }
       
        }
        
        
        backgroundView.addSubview(vw);
        window.addSubview(backgroundView)
        
    }
    

    func ShowInformation(AlertText : String)
    {
        
        let screenSize : CGRect = UIScreen.mainScreen().bounds;
        let FrameSize : CGRect = CGRectMake(0,0,screenSize.width,screenSize.height)
        let backgroundView : UIView = UIView(frame: FrameSize)
        backgroundView.tag = 1;
        backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        let window: UIWindow! = UIApplication.sharedApplication().delegate!.window!
        
        
        let vw: InformationView =  NSBundle.mainBundle().loadNibNamed("InformationView", owner: self, options: nil)[1] as! InformationView
        
        vw.frame=CGRectMake(20,((screenSize.size.height/2) - (vw.frame.size.height)/2), screenSize.size.width - 40, vw.frame.size.height )
        
        vw.btnClose.setTitle("Close", forState: UIControlState.Normal);
        
       
        if (AlertText != ""){
            vw.lblMessage.text = AlertText
        }
        
        
        backgroundView.addSubview(vw);
        window.addSubview(backgroundView)
        
    }
    
}
