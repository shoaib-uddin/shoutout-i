//
//  AddFeedViewController.swift
//  shoutOut
//
//  Created by clines193 on 6/21/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit

class AddFeedViewController: BaseViewController,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate, UITextViewDelegate ,AutoCompleteCellDelegate {
    
    //Page Controls
    
    @IBOutlet weak var lblUserTag: UILabel!
    @IBOutlet weak var btnFeedPost: UIButton!
    @IBOutlet weak var txtAreaFeedPost: UITextView!
    @IBOutlet weak var btnCameraFeedPost: UIStackView!
    @IBOutlet weak var imgProfilePicture: UIImageView!
    @IBOutlet weak var imgPictureUpload: UIImageView!
    @IBOutlet weak var imgBtnUpload: UIImageView!
    @IBOutlet weak var varScrollView: UIScrollView!
    @IBOutlet weak var lblCurrentDate: UILabel!
    @IBOutlet weak var btnToHide: UIButton!
    
    
    
    var AutoView: AutoCompleteView!;
    var info = UserModel.GetInfo();
    var pickedBase64Image : String = "";
    let recognizer = UITapGestureRecognizer();
    let imagePicker = UIImagePickerController();
    
    
    @IBAction func hideButton(sender: UIButton) {
        btnToHide.hidden = true
        txtAreaFeedPost.becomeFirstResponder()
    }
    
    
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Add Feed";
        txtAreaFeedPost.delegate = self;
        
        // CSSLayout called
        roundWhiteImageBorderWidthWise(imgProfilePicture);
        allSideWhiteBorder(btnFeedPost);
        allSideWhiteBorder(txtAreaFeedPost);
        
        // getting user variables
        let imagePath = SERVICEURL + info.image!;
        LazyImage.showForImageView(imgProfilePicture, url: imagePath);
        lblUserTag.text = info.user_tag;
        lblCurrentDate.text = String(NSDate());
        
        
        // image TAP
        imgBtnUpload.userInteractionEnabled = true
        recognizer.addTarget(self, action: #selector(SignUpPartTwoViewController.plusBtnTapped))
        imgBtnUpload.addGestureRecognizer(recognizer)
        
        // IMAGE DELEGATE
        imagePicker.delegate = self;
        
        
        
    }
    
    func plusBtnTapped(sender: UITapGestureRecognizer) {
        
        let alert = UIAlertController(title: "Select Picture Option", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let libButton = UIAlertAction(title: "Select photo from library", style: UIAlertActionStyle.Default) {  (alert) -> Void in
            
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.imagePicker.allowsEditing = true
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            let cameraButton = UIAlertAction(title: "Take a picture", style: UIAlertActionStyle.Default) {  (alert) -> Void in
                print("Take Photo")
                
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
                self.imagePicker.allowsEditing = true
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
                
            }
            alert.addAction(cameraButton)
        } else {
            print("Camera not available")
            
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {  (alert) -> Void in
            print("Cancel Pressed")
        }
        
        alert.addAction(libButton)
        alert.addAction(cancelButton)
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }

    
    //Image Picker Delegates
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        if let pickedImage = editingInfo![UIImagePickerControllerEditedImage] as? UIImage {
            
            imgPictureUpload.image = pickedImage
            pickedBase64Image = UtilityHelper.ImageToBase64String(pickedImage);
            
        }
        dismissViewControllerAnimated(true, completion: nil);
        
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            
            imgPictureUpload.image = pickedImage
            pickedBase64Image = UtilityHelper.ImageToBase64String(pickedImage);
            varScrollView.backgroundColor = UIColor.clearColor();
            
            print(pickedBase64Image);
            
            
        }
        dismissViewControllerAnimated(true, completion: nil);
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    
    
    @IBAction func FeedPostButtonClicked(sender: UIButton) {
        
        let loc = currLocation;
        
        // Initializing Items
        let FeedMsg: String = txtAreaFeedPost.text!  as String;
        let FeedImage: String = pickedBase64Image as String;
        
        
        //doing Validations
        if(!Validate.Required(FeedMsg, Label: "Feed Message")){return;}
        
        
        
        // Post Model Create
        let postModel =
            [ "message":  "\(FeedMsg)",
              "image":  "\(FeedImage)",
              "latitude" : "\(loc.coordinate.latitude)",
              "longitude" : "\(loc.coordinate.longitude)",
              "altitude" : "\(loc.altitude)" ];
        
        
        //Network Request
        NetworkHelper.MakePostRequest("shoutout_add_feed", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
           self.navigationController?.popViewControllerAnimated(false)
//            UtilityHelper.AlertMessagewithCallBack("Added Successfully", success: { })
            
            
            },failure: { (error) -> Void in
                var x = error;
            }
        )
        
    }
    
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        //
        let substring = (textView.text! as NSString).stringByReplacingCharactersInRange(range, withString: text);
        
        
        let i = range
        print(i);
        
        searchAutoCompleteHashtag(text, i: i)
        
        
        return true;
        
    }
    
    
    
    
    var starttagging: Bool = false;
    var searchString: String = "";
    var locat : Array<Int> = Array<Int>();
    
    let screenSize : CGRect = UIScreen.mainScreen().bounds;
    
    let window: UIWindow! = UIApplication.sharedApplication().delegate!.window!

    
    func searchAutoCompleteHashtag(substring:  String, i: NSRange){
        
        
        
        if((substring == "@" || starttagging == true) &&
            (substring != "" || substring != "\n" || substring != "\t" || substring != " ") ){
            
            if(AutoView == nil){
            AutoView =  NSBundle.mainBundle().loadNibNamed("AutoCompleteView", owner: self, options: nil)[0] as! AutoCompleteView
            let FrameSize : CGRect = CGRectMake(0,0,screenSize.width,screenSize.height)
            //AutoView.frame=CGRectMake(20,((screenSize.size.height/2) - (AutoView.frame.size.height)/2), screenSize.size.width - 40, AutoView.frame.size.height )
                
                
                
            AutoView.frame=CGRectMake(0, (screenSize.size.height/2)-100, screenSize.size.width, 200)
            
            AutoView.tag = 80
            AutoView.cellDelegate = self
            window.addSubview(AutoView)
             
                
                
            }
            
            txtAreaFeedPost.text = txtAreaFeedPost.text!
                starttagging = true
            
            
            }
        
        
        
        
        if(starttagging && substring != "@" && substring != " "){
            
                searchString += substring;
            
                locat.append(i.location);
            
            
            AutoView.setDataUserSearch(searchString)
            starttagging = true
            
        }
        
        
        
        
        if(substring == " "){
            
            starttagging = false
            searchString = "";
            print("tag end");
            debugPrint(locat);
            unloadView()
            
            
            
        }
        
        
        if(substring == "" || substring == "\n" || substring == "\t"){
            
            //txtAreaFeedPost.text = txtAreaFeedPost.text!.stringByReplacingOccurrencesOfString(searchString, withString: "")
            unloadView()
            starttagging = false
            
            searchString = "";
            print("tag end");
           
            
            
        
        }
        
        
        
        
        
        
    }
    
    
    
    
    
    
    func getHashtagOfUser(tag: String) {
        //txtAreaFeedPost.text = txtAreaFeedPost.text!.stringByReplacingOccurrencesOfString(searchString, withString: "")
        
        debugPrint(locat);
        replaceRangeOfText(tag)
        
        // Range: 3 to 7
        //txtAreaFeedPost.insertText(tag + " ");
        
        starttagging = false
        unloadView()
        
    }
    
    func unloadView(){
        if(AutoView != nil){
            AutoView.superview?.viewWithTag(80)?.removeFromSuperview();
            AutoView.removeFromSuperview()
            AutoView = nil;
            locat.removeAll();
        }

    }
    
    func replaceRangeOfText(tag:String){
    
        if(!locat.isEmpty){
            let startPosition = txtAreaFeedPost.positionFromPosition(txtAreaFeedPost.beginningOfDocument, inDirection: UITextLayoutDirection.Right, offset: locat.first!)
            let endPosition = txtAreaFeedPost.positionFromPosition(txtAreaFeedPost.beginningOfDocument, inDirection: UITextLayoutDirection.Right, offset: locat.last!+1)
            
            
            
            if startPosition != nil && endPosition != nil {
                txtAreaFeedPost.selectedTextRange = txtAreaFeedPost.textRangeFromPosition(startPosition!, toPosition: endPosition!);
                let selectedRange: UITextRange? = txtAreaFeedPost.selectedTextRange
                txtAreaFeedPost.replaceRange(selectedRange!, withText: tag + " ")
            }
            
            
        }

    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}