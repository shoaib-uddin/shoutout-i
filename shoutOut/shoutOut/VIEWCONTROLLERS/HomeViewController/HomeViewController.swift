		
//  HomeViewController.swift
//  ShoutOut
//
//  Created by Fez on 3/21/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit
import INTULocationManager
import MapKit

@objc  protocol HomeViewControlerDelegate {
            func searchCityText(searchText: String, actionType: String)
}
        
        
        
        
//Feed Controller known as Home
class HomeViewController: BaseViewController , UITabBarControllerDelegate ,  UIGestureRecognizerDelegate,  UISearchBarDelegate, AutoCompleteCellDelegate,TabButoonDelegate{
    
    
    @IBOutlet weak var lblspTopLabel: UILabel!
    @IBOutlet weak var placeHolderView: UIView!
    @IBOutlet weak var gotoAddFeedView: UIView!
    @IBOutlet weak var SearchbarCity: UISearchBar!
    @IBOutlet weak var ContainerViewForTable: UIView!
    @IBOutlet weak var ContainerViewForMap: UIView!
    
    var AutoView: AutoCompleteView!;
    var searchActive : Bool = false
    var searchCityText: String = "";
    weak var ViewDelegate: HomeViewControlerDelegate?
    
    
    @IBAction func ToggleShowSearch(sender: UIBarButtonItem) {
        if(SearchbarCity.hidden == true){
           SearchbarCity.hidden = false
        }else{
           clearSearchParameters()
        }
    }
    
    
    
    func TabbtnTapped(tag: Int, actionType: String) {
        //
        SearchbarCity.resignFirstResponder()
        print(tag);
        if tag == 0 {
            UIView.animateWithDuration(0.5, animations: {
                self.ContainerViewForMap.hidden = true
                self.ContainerViewForTable.hidden = false
            })
        } else {
            UIView.animateWithDuration(0.5, animations: {
                self.ContainerViewForMap.hidden = false
                self.ContainerViewForTable.hidden = true
            })
        }
        
    }
    
        
    
    override func viewWillDisappear(animated: Bool) {
        clearSearchParameters()
        
    }
    
    func appWillResignActive(not:NSNotification){
        
    }
    
    func appWillEnterForeGround(not:NSNotification){
        
    }
    
    override func viewDidLoad() {
        
        // change icon of search bar
        SearchbarCity.hidden = true
        SearchbarCity.delegate = self
        self.ContainerViewForMap.hidden = false
        self.ContainerViewForTable.hidden = true;
        
        
        // goto add feed 
        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.handleTap(_:)))
        tap.delegate = self;
        gotoAddFeedView.addGestureRecognizer(tap)
        
        // CSSLatout called
        spLabelTopTable(lblspTopLabel);
        
        placeHolderView.bringSubviewToFront(self.view);
        let vw: TabsView =  NSBundle.mainBundle().loadNibNamed("Tabs", owner: self, options: nil)[0] as! TabsView
        vw.frame=CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, placeHolderView.frame.size.height)
        vw.cellDelegate = self;
        placeHolderView.addSubview(vw);
        
        
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        UtilityHelper.NavigateToViewController(self, viewControllerid: "AddFeedViewController");
        
    }
    
    
    
    
    override func viewDidAppear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = false;
    }
    
    
        
       
    override func viewDidDisappear(animated: Bool) {
    }
        
    //
    //  implimenting search view in this page for filter table view 
    //
    
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
        unloadView();
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
        clearSearchParameters()
        unloadView();
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
        print(searchCityText);
        searchBar.resignFirstResponder()
        unloadView();
        
        ViewDelegate?.searchCityText(searchCityText, actionType: "")
        NSNotificationCenter.defaultCenter().postNotificationName("SearchCityNotification", object: searchCityText)
        clearSearchParameters();
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(!Validate.lettersNumbersOnly(searchText, Label: "")){
            return
        }
        print(searchText);
        unloadView()
        
        
        if(searchText.characters.count > 2){
            searchCityText = searchText
            openAutoView(searchCityText)
        }else{
            unloadView()
        }
    }
    
    let screenSize : CGRect = UIScreen.mainScreen().bounds;
    let window: UIWindow! = UIApplication.sharedApplication().delegate!.window!
    
    func openAutoView(searchCityText: String){
        
        if(AutoView == nil){
            AutoView =  NSBundle.mainBundle().loadNibNamed("AutoCompleteView", owner: self, options: nil)[0] as! AutoCompleteView
            //let FrameSize : CGRect = CGRectMake(0,0,screenSize.width,screenSize.height)
            //AutoView.frame=CGRectMake(20,((screenSize.size.height/2) - (AutoView.frame.size.height)/2), screenSize.size.width - 40, AutoView.frame.size.height )
            
            AutoView.frame=CGRectMake(0, 110, screenSize.size.width, 200)
            AutoView.tag = 80
            AutoView.cellDelegate = self
           
            AutoView.setDataCitySearch(searchCityText) { (callback) in
                //
                if(callback && self.AutoView != nil){
                    self.window.addSubview(self.AutoView)
                }
            }
        
        2
        
        }
        
        
        
    }
    
    
    func unloadView(){
        
        if(AutoView != nil){
            AutoView.superview?.viewWithTag(80)?.removeFromSuperview();
            AutoView.removeFromSuperview()
            AutoView = nil;
            //SearchbarCity.resignFirstResponder()
            
        }
        
    }

    
    func getHashtagOfUser(tag: String) {
        //
        SearchbarCity.text = tag;
        searchCityText = tag;
        unloadView()
        ViewDelegate?.searchCityText(searchCityText, actionType: "")
        
    }
    
    func clearSearchParameters(){
        SearchbarCity.hidden = true
        SearchbarCity.text = ""
        SearchbarCity.resignFirstResponder()
    
    }
    
    
    
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        self.view!.endEditing(true)
//    }
    
    
    
    
    
    
    
    
 
    

}

