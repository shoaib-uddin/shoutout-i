//
//  HomeViewTableViewController.swift
//  shoutOut
//
//  Created by clines193 on 7/27/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit
import INTULocationManager


@objc  protocol FeedTableViewDelegate {
    //func TabbtnTapped(tag: Int, actionType: String)
}

//Feed Controller known as Home
class HomeViewTableViewController: BaseViewController, UITableViewDataSource , UITableViewDelegate, FeedlistCellDelegate, FeedImagelistCellDelegate, HomeViewControlerDelegate {
    
   // weak var ViewDelegate: HomeViewControlerDelegate?
    var feedList : Array<FeedModel> = Array<FeedModel>();
    var locMgr: INTULocationManager = INTULocationManager.sharedInstance();
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        tableView.registerNib(UINib(nibName: "FeedTextTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedTextTableViewCell")
        tableView.registerNib(UINib(nibName: "FeedImageTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedImageTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
         // table CSS
        tableView.estimatedRowHeight = 400;
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.backgroundView?.backgroundColor = UIColor.clearColor();
        tableView.backgroundColor = UIColor.clearColor();
        
        getLocationAllLoadFeed();
       
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewTableViewController.methodOFReceivedNotication(_:)), name:"SearchCityNotification", object: nil)
        
        
        
    }
    
    func methodOFReceivedNotication(notification: NSNotification){
        let searchString = notification.object as! String
        getFeedByCityName(searchString)
  
    }
    
    
    func getLocationAllLoadFeed(){
        
        
        if(currLocation == nil){
        locMgr.requestLocationWithDesiredAccuracy(INTULocationAccuracy.City, timeout: 3) { (loc, Accuracy, _Status) in
            
            currLocation  = loc;
            self.get_all_feeds();
            
        }
        }
        else{
            self.get_all_feeds();
            
        }
    }
    
    /*
     *  functions to Delete a feed of table cell and send to server
     *
     **/
    
    
    func DeleteMyShoutout(cell: FeedTextTableViewCell, actionType: String) {
        //
        DeleteThisFeed(self.feedList[cell.indexPathRow].id!, index: cell.indexPathRow)
    }
    
    func DeleteMyImageShoutout(cell: FeedImageTableViewCell, actionType: String) {
        //
        DeleteThisFeed(self.feedList[cell.indexPathRow].id!, index: cell.indexPathRow)
       
    }
    
    func DeleteThisFeed(feed_id: String, index: Int){
        
        let alert = UIAlertController(title: "Delete Feed", message: "Are you sure?" , preferredStyle: UIAlertControllerStyle.Alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: { action in
            
            // do something like...
            ShoutoutAPI.DeleteShoutoutFromServer(feed_id, completion: {(callback) in
                self.feedList.removeAtIndex(index);
                self.tableView.reloadData();
            })
            
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { action in
            
            // do something like...
            print("cancel")
            
        }))
        
        // show the alert
        self.presentViewController(alert, animated: true, completion: nil)
    
    }
    
    
    /*
     *  functions to like a feed of table cell and send to server
     *
     **/
    
    var isFeedLiked:Bool = false;
    
    func isFeedLiked(cell: FeedTextTableViewCell, actionType: String) {
        cell.setFeedLiked(self.feedList[cell.indexPathRow])
    }
    
    func isFeedImageLiked(cell: FeedImageTableViewCell, actionType: String) {
        cell.setFeedLiked(self.feedList[cell.indexPathRow])
    }
    
    
    /*
     *  functions to transfer user to comments section of table cell
     *
     **/
    
    func showComments(cell: FeedTextTableViewCell, actionType: String) {
        showSignleFeedComments(cell.FeedID!, index: cell.indexPathRow)
    }
    
    func showImageComments(cell: FeedImageTableViewCell, actionType: String){
        showSignleFeedComments(cell.FeedID!, index: cell.indexPathRow)
    }
    
    func showSignleFeedComments(feed_id: String, index: Int){
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let destination = storyboard.instantiateViewControllerWithIdentifier("CommentFeedViewController") as! CommentFeedViewController
        destination.feed_id = feed_id;
        destination.feed = feedList[index];
        self.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    /*
     *  functions to make user follow the feed of table cell
     *
     **/
    
    func followThisFeed(cell: FeedTextTableViewCell, actionType: String) {
        cell.setFeedFollow(self.feedList[cell.indexPathRow]);
    }
    
    func followThisImageFeed(cell: FeedImageTableViewCell, actionType: String) {
        cell.setFeedFollow(self.feedList[cell.indexPathRow]);
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        // call a function to get feeds
        
        
            getLocationAllLoadFeed();
    }
    

    func get_all_feeds(){
        
        let loc = currLocation;
        
        
        // Post Model Create
        let postModel = ["start_record" : "0",
                         "end_record" : 18,
                         "latitude" : "\(loc.coordinate.latitude)",
                         "longitude" : "\(loc.coordinate.longitude)"];
        
        //Network Request
        NetworkHelper.MakePostRequestForArray("shoutout_get_all_feeds", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
            if(successData != "" && successData != "[\n\n]"){
                
                self.feedList = [FeedModel](json: successData);
                self.tableView.reloadData();
                
            }else{
                //UtilityHelper.AlertMessage("No feed found")
                self.tableView.reloadData();
            }

            
            
            },failure: { (error) -> Void in
                _ = error
                
        })
        
    }
    
    //Delegation Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedList.count;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        showSignleFeedComments(feedList[indexPath.row].id!, index: indexPath.row);
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == self.feedList.count - 1 {
            self.loadMore(indexPath.row + 1);
        }
        
        if(feedList[indexPath.row].image == ""){
            
            let cell = tableView.dequeueReusableCellWithIdentifier("FeedTextTableViewCell", forIndexPath: indexPath) as! FeedTextTableViewCell
            cell.indexPathRow = indexPath.row;
            cell.setData(feedList[indexPath.row], myshoutout: false);
            cell.cellDelegate = self;
            return  cell;
            
        }
        else{
            
            let cell = tableView.dequeueReusableCellWithIdentifier("FeedImageTableViewCell", forIndexPath: indexPath) as! FeedImageTableViewCell
            cell.indexPathRow = indexPath.row;
            cell.setData(feedList[indexPath.row], myshoutout: false);
            cell.cellDelegate = self;
            return  cell;
            
        }
        
        
    }
    
    
    
    func loadMore(lastCount: Int){
        
        let pre = String(lastCount + 1);
        let next = lastCount + 10;
        let loc = currLocation;
        
        let postModel = ["start_record" : pre,
                         "end_record" : next,
                         "latitude":"\(loc.coordinate.latitude)",
                         "longitude": "\(loc.coordinate.longitude)"];
        
        //Network Request
        NetworkHelper.MakePostRequestForArray("shoutout_get_all_feeds", postData: postModel, showLoader: false, success: { (successData) -> Void in
            
            if(successData != "" && successData != "[\n\n]"){
                
                var f = [FeedModel](json: successData);
                self.feedList.appendContentsOf(f);
                self.tableView.reloadData();
                
            }
            
            },failure: { (error) -> Void in
                _ = error
                
        })
        
        
        
    }
    
    func searchCityText(searchText: String, actionType: String) {
        //
        getFeedByCityName(searchText)

    }
    
    func getFeedByCityName(searchCityText: String){
        
        
        let postModel = ["city_name": searchCityText,
                         "start_record" : 0,
                         "end_record" : 100];
        
        //Network Request
        NetworkHelper.MakePostRequestForArray("shoutout_get_city_wise_feed", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
            
            
            if(successData != "" && successData != "[\n\n]"){
                
                self.feedList.removeAll();
                var f = [FeedModel](json: successData);
                self.feedList.appendContentsOf(f);
                self.tableView.reloadData();
                
            }else{
                //UtilityHelper.AlertMessage("No Feed Found");
                self.feedList.removeAll();
                self.tableView.reloadData();
            }
            
            
            },failure: { (error) -> Void in
                _ = error
                
        })
        
        
    }
    

    
    
    
    
    
    
    
    
}

