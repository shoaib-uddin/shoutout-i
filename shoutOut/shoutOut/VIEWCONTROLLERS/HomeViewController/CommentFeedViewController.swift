//
//  AddFeedViewController.swift
//  shoutOut
//
//  Created by clines193 on 6/21/16.
//  Copyright © 2016 Matech. All rights reserved.
//

import Foundation
import UIKit

class CommentFeedViewController: BaseViewController,
UINavigationControllerDelegate, UITabBarControllerDelegate,
UITableViewDataSource , UITableViewDelegate ,
FeedlistCellDelegate, FeedImagelistCellDelegate, CommentlistCellDelegate, UITextFieldDelegate, UITextViewDelegate
{
    
    var FeedComments : Array<CommentModel> = Array<CommentModel>();
    
    var feed : FeedModel!;
    var feed_id : String?;
    var user = UserModel.GetInfo();
    var feedList : Array<FeedModel> = Array<FeedModel>();
    var isFeedLiked:Bool = false;
    
    
    func DeleteMyComment(cell: commentsTableViewCell, actionType: String) {
        //
        // create the alert
        
        ShoutoutAPI.DeleteCommentFromServer(FeedComments[cell.indexPathRow].id!, completion: {(callback) in
            
            if(callback){
                self.tableView.scrollToRowAtIndexPath(self.getLastIndexPath(), atScrollPosition: .Bottom, animated: false);
                self.tableView.beginUpdates();
                self.FeedComments.removeAtIndex(cell.indexPathRow);
                self.tableView.deleteRowsAtIndexPaths(NSArray(object: NSIndexPath(forRow: cell.indexPathRow, inSection: 1)) as! [NSIndexPath], withRowAnimation: UITableViewRowAnimation.Left)
                self.tableView.endUpdates()
            
            }
            
        })
        
                
    }
    
    
    
    /*
     *  functions to Delete a feed of table cell and send to server
     *
     **/
    
    
    func DeleteMyShoutout(cell: FeedTextTableViewCell, actionType: String) {
        //
        DeleteThisFeed(self.feedList[cell.indexPathRow].id!, index: cell.indexPathRow)
    }
    
    func DeleteMyImageShoutout(cell: FeedImageTableViewCell, actionType: String) {
        //
        DeleteThisFeed(self.feedList[cell.indexPathRow].id!, index: cell.indexPathRow)
        
    }
    
    func DeleteThisFeed(feed_id: String, index: Int){
        
        let alert = UIAlertController(title: "Delete Feed", message: "Are you sure?" , preferredStyle: UIAlertControllerStyle.Alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: { action in
            
            // do something like...
            ShoutoutAPI.DeleteShoutoutFromServer(feed_id, completion: {(callback) in
                self.dismissViewControllerAnimated(true, completion: nil)
            })
            
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { action in
            
            // do something like...
            print("cancel")
            
        }))
        
        // show the alert
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

    
    
    
    
    func isFeedLiked(cell: FeedTextTableViewCell, actionType: String) {
        cell.setFeedLiked(self.feedList[cell.indexPathRow])
    }
    
    
    func isFeedImageLiked(cell: FeedImageTableViewCell, actionType: String) {
        cell.setFeedLiked(self.feedList[cell.indexPathRow])
    }
    
    
    /*
     *  functions to transfer user to comments section of table cell
     *
     **/
    
    func showComments(cell: FeedTextTableViewCell, actionType: String) {
        
    }
    
    func  showImageComments(cell: FeedImageTableViewCell, actionType: String){
        
    }
    
    
    /*
     *  functions to make user follow the feed of table cell
     *
     **/
    
    func followThisFeed(cell: FeedTextTableViewCell, actionType: String) {
        cell.setFeedFollow(self.feedList[cell.indexPathRow]);
    }
    
    func followThisImageFeed(cell: FeedImageTableViewCell, actionType: String) {
        cell.setFeedFollow(self.feedList[cell.indexPathRow]);
    }

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var placeHolderView: UIView!
    
    //Page Controls
    @IBOutlet weak var imgProfilePicture: UIImageView!
    @IBOutlet weak var lblUserTag: UILabel!
    @IBOutlet weak var txtAreaFeedPost: UITextView!
    @IBOutlet weak var imgPictureUpload: UIImageView!
    @IBOutlet weak var tbxCommentFeed: UITextField!
    @IBOutlet weak var imgFeedPost: UIImageView!
    @IBOutlet weak var btnCommentFeedPost: UIButton!
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        self.title = "comments";
        
        tbxCommentFeed.delegate = self;
        
        // CSSLayout called
        //setUITextViewCons(tbxCommentFeed, icon: "user-seperator@1x", showIcon: false);
        //roundWhiteImageBorder(imgProfilePicture)
        
        
        // setting user variables
        let imagePath = SERVICEURL + feed.user_image!;
        // LazyImage.showForImageView(imgProfilePicture, url: imagePath);
        //lblUserTag.text = feed.user_tag;
        
        
        tableView.registerNib(UINib(nibName: "commentsTableViewCell", bundle: nil), forCellReuseIdentifier: "commentsTableViewCell")
        tableView.registerNib(UINib(nibName: "FeedImageTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedImageTableViewCell")
        tableView.registerNib(UINib(nibName: "FeedTextTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedTextTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.backgroundView?.backgroundColor = UIColor.clearColor();
        tableView.backgroundColor = UIColor.clearColor();
        
        tableView.estimatedRowHeight = 400;
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        // table CSS
        //tableView.backgroundView?.backgroundColor = UIColor.clearColor();
        //tableView.backgroundColor = UIColor.clearColor();
        
        // hide tab bar for this view
        self.tabBarController?.tabBar.hidden = true
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        // call a function to get feeds
        
        self.feedList.append(feed);
        
        
        
        //Network Request
        NetworkHelper.MakeGetRequestForArray("shoutout_get_feed_comments/" + feed_id! + "/", showLoader: true, success: { (successData) -> Void in
            
            
            
            if(successData != "" && successData != "[\n\n]"){
                
                let response =  [CommentModel](json: successData)
                self.FeedComments = response;
                self.tableView.reloadData();
               
            }
            },failure: { (error) -> Void in
                var x = error;
                
            }
        )

        
    }

    
    
    
    
    override func viewDidAppear(animated: Bool) {
        
        
        // CSSLatout called
        
        
        
        

        
     
        
    }

    
    //Delegation Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if( section == 0){ return 1; }
        else{ return FeedComments.count; }
        
    }
    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        
//        
//        
//        
//    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 2;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
        if (indexPath.section==0) {
            
            
            if(feed.image == ""){
                let cell = tableView.dequeueReusableCellWithIdentifier("FeedTextTableViewCell", forIndexPath: indexPath) as! FeedTextTableViewCell
                cell.backgroundColor = UIColor.clearColor();
                cell.backgroundView?.backgroundColor = UIColor.clearColor();
                cell.indexPathRow = indexPath.row;
                cell.setData(feed, myshoutout: false);
                cell.cellDelegate = self;
                return cell;
                
            }
            else{
                let cell = tableView.dequeueReusableCellWithIdentifier("FeedImageTableViewCell", forIndexPath: indexPath) as! FeedImageTableViewCell
                cell.backgroundColor = UIColor.clearColor();
                cell.backgroundView?.backgroundColor = UIColor.clearColor();
                cell.indexPathRow = indexPath.row;
                cell.setData(feed, myshoutout: false);
                cell.cellDelegate = self;
                return cell;
            
            }
            
            
            
            
        
        
        }
        else {
        
            let cell = tableView.dequeueReusableCellWithIdentifier("commentsTableViewCell", forIndexPath: indexPath) as! commentsTableViewCell
            cell.indexPathRow = indexPath.row;
            cell.setData(FeedComments[indexPath.row], id: user.user_id!);
            cell.cellDelegate = self;
            return cell;
            
        }
        
        
    }
    
    
    func getLastIndexPath() -> NSIndexPath{
        //let _section : Int = self.FeedComments.count + 1;
        let indexPath = NSIndexPath(forRow:  self.FeedComments.count - 1 , inSection: 1)
        return indexPath;
    }
    
    
    // sence the hashtag in the textbox
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        //detect a hash
        
        let substring = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string);
        
        // searchAutoCompleteHashtag(substring)
        
        
        return true;
        
    }
        
    
    var starttagging: Bool = false;
    var searchString: String = "";
    
    func searchAutoCompleteHashtag(substring:  String){
        
        var hashtagArray = [String]();
        var mentionArray = [String]();
        
        var inindex:Int = 0;
        var neindex:Int = 0;
        
        if(substring.characters.last == "#" || starttagging == true ){
            
            starttagging = true
            var characterNameLast: Character = substring.characters.last!
            searchString += String(characterNameLast);
            print(searchString);
            
            
        }
        
        if(substring.characters.last == " "){
            
            starttagging = false
            searchString = "";
            print("tag end");
            
        }
        
        
    }
    
    
    
    
    @IBAction func FeedPostButtonClicked(sender: UIButton) {
        
        
        
        // Initializing Items
        let FeedComment: String = tbxCommentFeed.text!  as String;
        let feed_id: String = feed.id! as String;
        
        
        //doing Validations
        if(!Validate.Required(FeedComment, Label: "comment")){return;}
        
        
        // Post Model Create
        let postModel =
            [ "comment" : FeedComment,
              "feed_id" : feed_id
              ];
        
        
       //Network Request
        NetworkHelper.MakePostRequest("shoutout_add_comment/", postData: postModel, showLoader: true, success: { (successData) -> Void in
            
            let response : CommentIdModel = CommentIdModel(dictionary: successData as! NSDictionary);
            
            self.view.endEditing(true);
            var fdComment : CommentModel = CommentModel();
            fdComment.feed_id = feed_id;
            fdComment.id = "\(response.comment_id!)";
            fdComment.created_at = "now";
            fdComment.last_name = self.user.last_name!;
            fdComment.user_id = self.user.user_id;
            fdComment.updated_at = "";
            fdComment.comment = FeedComment;
            fdComment.first_name = self.user.first_name!;
            fdComment.image = self.user.image;
            
            
            self.FeedComments.append(fdComment);
       
            
            self.tableView.beginUpdates()
            
            
            self.tableView.insertRowsAtIndexPaths( [self.getLastIndexPath()], withRowAnimation: .Automatic)
            
            self.tbxCommentFeed.text = "";
            
            self.tableView.endUpdates()
            
            //let indexPath = NSIndexPath(forRow: self.FeedComments.count-1, inSection: 1)
            self.tableView.scrollToRowAtIndexPath(self.getLastIndexPath(), atScrollPosition: .Bottom, animated: false);
            
            
            },failure: { (error) -> Void in
                var x = error;
            }
        )
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}